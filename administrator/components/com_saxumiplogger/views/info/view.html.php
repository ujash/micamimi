<?php
/**
 * $Id: view.html.php 10 2013-11-10 22:53:48Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view' );

class SaxumiploggerViewInfo extends JViewLegacy
{
	function display($tpl = null)
	{	
		$mainframe = JFactory::getApplication();
		$option = JRequest::getCmd('option');
		JHTML::stylesheet( 'saxumpicker.css', 'administrator/components/com_saxumpicker/' );
		SaxumiploggerController::addSubmenu('info');

		jimport('joomla.html.pagination');
		
		JToolBarHelper::title(JText::_( 'COM_SAXUMIPLOGGER' ).' - '.JText::_( 'COM_SAXUMIPLOGGER_INFORMATION' ), 'info' );			
		JToolBarHelper::cancel( 'cancel', 'Close' );
		$this->sidebar = JHtmlSidebar::render();
		
		JHTML::_('behavior.tooltip', '.hasTip');
		$file		= JPATH_COMPONENT.DIRECTORY_SEPARATOR.'saxumiplogger.xml';
		$xml		= JFactory::getXML($file);
		$version=(string)$xml->version;

		$this->assign( 'version'	, $version );

		parent::display( $tpl );
	}
}

