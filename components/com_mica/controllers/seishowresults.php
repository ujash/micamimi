<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('MicaController', JPATH_COMPONENT . '/controller.php');

//DEVNOTE: import CONTROLLER object class
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/Workspace.php' );
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/css.php' );
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/pdf.php' );

/**
 * SEIShowresults controller class for MICA.
 *
 * @since  1.6
 */
class MicaControllerSEIShowresults extends MicaController
{
	//Custom Constructor
	var $tomcat_url             = "";
	var $townsearchvariable     = "OGR_FID";
	var $statesearchvariable    = "OGR_FID";
	var $urbansearchvariable    = "OGR_FID";
	var $districtsearchvariable = "OGR_FID";
	var $ruralprefix            = "Rural_";
	var $urnbanprefix           = "Urban_";
	var $totalprefix            = "Total_";
	var $_modal                 = "";

	function __construct($default = array()){
		parent::__construct($default);

		$session     = JFactory::getSession();

		$user = JFactory::getUser();
		if ($user->id == 0) {
			$session->set('user_id',null);
		}
		else{
			$session->set('user_id', $user->id);
		}

		if(!defined('TOMCAT_URL')){
			$this->getTomcatUrl();
			DEFINE('TOMCAT_URL', $this->tomcat_url);
		}
		//DEFINE('TOMCAT_STYLE_ABS_PATH', "/usr/share/tomcat7/webapps/geoserver/data/www/styles/");
		//DEFINE('TOMCAT_STYLE_ABS_PATH', "/home/mimi/apache-tomcat-7-0-26/webapps/geoserver/data/www/styles/");
		DEFINE('TOMCAT_STYLE_ABS_PATH' ,"/var/www/html/micamimi18/geoserver/www/styles/micamimi18/");
		if(!defined('TOMCAT_STYLE_ABS_PATH')){
			//DEFINE('TOMCAT_STYLE_ABS_PATH' ,"/usr/local/easy/share/easy-tomcat7/webapps/geoserver/data/www/styles/");
			//DEFINE('TOMCAT_STYLE_ABS_PATH' , JPATH_SITE."/geoserver/data/www/styles/");
			DEFINE('TOMCAT_STYLE_ABS_PATH' ,"/usr/local/easy/share/easy-tomcat7/webapps/geoserver/data/www/styles/micamimi18/");
			DEFINE('TOMCAT_STYLE_ABS_PATH' ,"/var/www/html/micamimi18/geoserver/www/styles/");
		}

		$this->_table_prefix = '#__mica_';

		// EDITED BY SALIM STARTED 25-10-2018..

		require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/Workspace.php' );
		require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/css.php' );
		$model = $this->getModel('seishowresults');

		if(!defined('TOMCAT_URL')){
			$this->getTomcatUrl();
			DEFINE('TOMCAT_URL', $this->tomcaturl);
		}
		DEFINE('TOMCAT_STYLE_ABS_PATH' , JPATH_SITE."/geoserver/data/www/styles/micamimi18/");

		if(!defined('TOMCAT_STYLE_ABS_PATH')){
			//DEFINE('TOMCAT_STYLE_ABS_PATH' ,"/usr/local/easy/share/easy-tomcat7/webapps/geoserver/data/www/styles/");
			//DEFINE('TOMCAT_STYLE_ABS_PATH' , JPATH_SITE."/geoserver/data/www/styles/");
			//DEFINE('TOMCAT_STYLE_ABS_PATH' ,"/usr/local/easy/share/easy-tomcat7/webapps/geoserver/data/www/styles/micamimi18/");
			DEFINE('TOMCAT_STYLE_ABS_PATH' , JPATH_SITE."/geoserver/data/www/styles/micamimi18/");
		}

		if(!defined('TOMCAT_SLD_FOLDER')){
			//DEFINE('TOMCAT_SLD_FOLDER' ,"geoserver/www/styles");
			DEFINE('TOMCAT_SLD_FOLDER' ,"geoserver/www/styles/micamimi18/");
		}

		$app = JFactory::getApplication();

		$msg = $app->input->get('msg');
		if($msg == "-1"){
			$app->enqueueMessage(JTEXT::_('WORKSPACE_DELETED') );
		}else if($msg=="0"){
			$app->enqueueMessage(JTEXT::_('WORKSPACE_CREATED') );
		}else if($msg=="1"){
			$app->enqueueMessage(JTEXT::_('WORKSPACE_UPDATED') );
		}

		//$refererview = $app->input->get("refeterview");
		$apply_chnages_val = $app->input->get("apply_chnages_val");
		if($apply_chnages_val == 1){
			$session->set('oldattributes',"");
			$session->set('attributes',null);
			$session->set('state',null);
			$session->set('district',null);
			$session->set('urban',null);
			$session->set('town',null);
			// Comment this code
			$session->set('activeworkspace',null);
			$session->set('attrname',null);
			$session->set('customattribute',null);
			$session->set('m_type',null);
			$session->set('activetable',null);
			$session->set('activedata',null);
			$session->set('composite',null);
			$session->set('m_type_rating',null);
			//if($app->input->get('type') == 'gis'){
			$session->set('customformula',null);
			//}
			$session->set('Groupwisedata',null);
			$session->set('popupactivedata',null);
			$session->set('themeticattribute',null);
			$session->set('activenamevariable',null);
			$session->set('activesearchvariable',null);
			$session->set('dataof',null);
			$session->set('customattributelib',null);
		}

		$session->set('m_type_rating', $app->input->get('m_type_rating', '', 'raw'));

		if($session->get('activeworkspace') == ""){

			$result = workspaceHelper::loadWorkspace(1);

	    	if(!empty($result)){

	    		$session->set('activeworkspace',$result[0]['id']);
				$innercall = 1;
				$session->set('gusetprofile',$result[0]['id']);
				$session->set('is_default',"1");
				$app->input->set('activeworkspace', $session->get('activeworkspace'));
				cssHelper::flushOrphandSld($session->get('activeworkspace'));

	    	}else{

				$name      = $app->input->set("name", "Guest");
				$default   = $app->input->set("default", 1);
				$innercall = 1;
				$id        = workspaceHelper::saveWorkspace($innercall);
				$session->set('gusetprofile', $id);
				$session->set('activeworkspace', $id);
				$session->set('is_default',"1");
	    	}

		}else if($session->get('is_default') == 1){

	    	workspaceHelper::updateWorkspace($innercall);

	    }else{

    	    $app->input->set('activeworkspace', $session->get('activeworkspace'));
			//$this->saveSldToDataBase("1","1");
	    }

		$state = $app->input->get('state', '', 'raw');

		$stats = array();
		foreach($state as $eachstat){
			$stats[] = base64_decode($eachstat);
		}
		$state = $stats;
		if(is_array($state)){
			$state = implode(",",$state);
			$app->input->set('state',$state);
		}

		$sessionstate = $session->get('state');
		$app->input->set('state', $sessionstate);
		if($sessionstate != ""){
			$addtosession = ($sessionstate.",".$state);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp);
			$sessionstate = implode(",",$mytmp);
		}else{
			$sessionstate = $state;
		}
		$session->set('state', $sessionstate);

		// Set  District in Session
		$district = $app->input->get('district', '', 'raw');
		if(is_array($district)){
			$district = implode(",", $district);
			$app->input->set('district', $district);
		}

		$sessiondistrict = $session->get('district');
		if($sessiondistrict != ""){
			$addtosession1   = ($sessiondistrict.",".$district);
			$tmp             = explode(",",$addtosession1);
			$mytmp           = array_unique($tmp);
			$mytmp           = array_filter($mytmp);
			$sessiondistrict = implode(",",$mytmp);
		}else{
			$sessiondistrict = $district;
		}
		$app->input->set('district',$sessiondistrict);
		$session->set('district',$sessiondistrict);

		$m_type = $app->input->get('m_type', '', 'raw');
		//$session_mtype=($session->get('m_type'));
		if(!is_array($m_type)){
			$m_type = $session->get('m_type');
		}else{
			$session->set('m_type', $m_type);
			$m_type = $session->get('m_type');
		}

		$town        = $app->input->get('town', '', 'raw');
		$sessiontown = $session->get('town');
		if($sessiontown != ""){
			$addtosession =($sessiontown.",".$town);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp);
			$sessiontown  = implode(",",$mytmp);
		}else{
			$sessiontown = $town;
		}
		$session->set('town',$sessiontown);
		$app->input->set('town',$sessiontown);

		$urban        = $app->input->get('urban', '', 'raw');
		$sessionurban = $session->get('urban');
		if($sessionurban !="" ){
			$addtosession = ($sessionurban.",".$urban);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$sessionurban = implode(",",$mytmp);
			$session->set('dataof',"Urban");
		}else{
			$sessionurban = $urban;
		}
		$session->set('urban',$sessionurban);
		$app->input->set('urban',$sessionurban);

		if($sessiontown != ""){
			$app->input->set('db_table','Town');
			$zoom = 9;//JRequest::setVar('db_table','Town');
		}else if($sessionurban != ""){
			$app->input->set('db_table','Urban');
			$zoom = 8;//JRequest::setVar('db_table','Urban');
		}else if($sessiondistrict != ""){
			$app->input->set('db_table','District');
			$zoom = 7;//JRequest::setVar('db_table','District');
		}else{
			$app->input->set('db_table','State');
			$zoom = 6;//JRequest::setVar('db_table','State');
		}

		$dataof = $app->input->get('dataof', '', 'raw');
		if($dataof == ""){
			$dataof = $session->get('dataof');
			//$app->input->get('dataof',$dataof);
		}else{
			$session->set('dataof',$dataof);
		}


		$composite = $app->input->get('composite', '', 'raw');
		if(!empty($composite)){
			$session->set('composite', $app->input->get('composite', '', 'raw'));
			//$attributes = JRequest::getVar('attributes');
			$composite = array_merge(array("MPI"), $composite);
		}else{
			$session->clear('composite');
		}


		$attributes = $app->input->get('attributes', '', 'raw');
		if(!empty($attributes)){
			$composite = $session->get('composite');
		    //echo  '<br/>==<br/>'.implode(",",$composite);
		    //echo  '<br/>==<br/>'.implode(",",$attributes);
			//$attributes = JRequest::getVar('attributes');
			if(!empty($composite)){
				$attributes = array_merge(array("MPI"),$attributes,$composite);
			}else{
				$attributes = array_merge(array("MPI"),$attributes);
			}
			//echo  '<br/>==<br/>'.implode(",",$attributes);
		}
		//echo "a<pre>";print_r($attributes);exit;
		if(is_array($attributes)){
			$attributes = array_filter($attributes);
			$attributes = implode(",",$attributes);
		}
		$sessionattr = $session->get('attributes');
		if($sessionattr != ""){
			if($app->input->get('attributes', '', 'raw') != ""){
				$addtosession = ($attributes);
			}else{
				$addtosession = ($sessionattr);
			}
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$addtosession = implode(",",$mytmp);
		}else{
			$addtosession = $attributes;
		}
		$session->set('attributes',$addtosession );
		$app->input->set('sessionattributes',$addtosession);

		$state_items = $model->getStateItems();


		$result['dataof']                 = $dataof;
		$result['state_items']            = $state_items;
		$result['townsearchvariable']     = $this->townsearchvariable;
		$result['urbansearchvariable']    = $this->urbansearchvariable;
		$result['districtsearchvariable'] = $this->districtsearchvariable;

		$this->Itemid = $app->input->get('Itemid', 188);

		$customattribute  = $session->get('customattribute');
		//set custom attribut session
		$attributes   = $this->input->get('custom_variable', '', 'raw');
		foreach ($attributes as $key => $value) {
		 	$attrname = $key;
		 	$customattribute   = $attrname.":".$value.",".$customattribute;
		}

	 	$session->set('customattribute',$customattribute);
	 	$customattribute  = $session->get('customattribute');

		//set custom attribut session ends
		$new_name         = $app->input->get('new_name', array(), 'array');
		$custom_attribute = $app->input->get('custom_attribute', array(), 'array');

		$customdata = $model->getCustomData();

		//$MaxForMatrix= $this->get('MaxForMatrix');
		//$this->MaxForMatrix = $MaxForMatrix;
		$session->set('customdata', $customdata);
		$this->customdata = $customdata;
		$this->state      = $sessionstate;
		$this->district   = $sessiondistrict;
		$this->town       = $sessiontown;
		$this->urban      = $sessionurban;
		$this->zoom       = $zoom;

		$page = $app->input->get("page");

		$AttributeTable  = $this->get('NewAttributeTable');// $this->get('AttributeTable');
		$graph           = $this->get('Graph');
		$GraphSettings   = $this->get('GraphSettings');

		$activesearchvariable       = $session->get('activesearchvariable');
		$this->activesearchvariable = $activesearchvariable;
		$this->zoom                 = $zoom;
		/*$tomcaturl                  = TOMCAT_URL;
		$this->tomcaturl            = $tomcaturl;*/

		$BreadCrumbcontent = $this->get('BreadCrumb');
		list($BreadCrumb, $workspacelink) = explode("~~~", $BreadCrumbcontent);
		$this->workspacelinkdiv   = $workspacelink;
		$this->BreadCrumb         = $BreadCrumb;

		$geometry                 = $model->getInitialGeometry();
		$UnselectedDistrict       = $model->getUnselectedDistrict();
		$this->UnselectedDistrict = $UnselectedDistrict;
		$this->geometry           = $geometry;

		$this->user               = JFactory::getUser();
		$this->userid             = $this->user->id;
		$this->activeworkspace    = $session->get('activeworkspace');
		$this->customattribute    = $customattribute;

		$this->get('customattrlib');
		$model->getcustomattrlib();
		$this->customattributelib = $session->get('customattributelib');
		$this->AttributeTable     = $AttributeTable;
		$this->GraphSettings      = $GraphSettings;
		$this->graph              = $graph;

		/*$this->m_type = $m_type;$app->input->set('m_type',$m_type);*/
		$this->attributes        = $addtosession;
		$this->new_name          = $new_name;
		$this->custom_attribute  = $custom_attribute;

		$pagination = $this->get('Pagination');
		$this->pagination        = $pagination;
		$this->themeticattribute = $session->get('themeticattribute');

		//if($app->input->get('type') == 'thematic' || $app->input->get('task') == 'addthematicQueryToSession'){
			//NOTE :: 30-03-2019 comment this code because it's issue in create workspace, update last workspace name also
			//workspaceHelper::updateWorkspace($innercall);
			cssHelper::saveSldToDatabase(1, 1);
		//}
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'SEIShowresults', $prefix = 'MicaModel', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}

	/**
	 * ajax task.
	 */
	/*public function deleteattribute(){
		$session    = JFactory::getSession();
		$removeattr = $this->input->get('attr', '', 'raw');

		$removeattrs[] = "rural_".str_replace(" ","_",strtolower($removeattr));
		$removeattrs[] = "urban_".str_replace(" ","_",strtolower($removeattr));
		$removeattrs[] = "total_".str_replace(" ","_",strtolower($removeattr));
		$removeattrs[] = str_replace(" ","_",strtolower($removeattr));
		$removeattr    = str_replace(" ","_",$removeattr);

		$attributes = $session->get('attributes');

		$original   = strlen($attributes);
		foreach($removeattrs as $removeattr){
			$attributes = str_ireplace($removeattr, "", $attributes);
		}

		$attributes = explode(",", $attributes);
		$attributes = array_filter($attributes);
		$attributes = implode(",", $attributes);

		$removed = strlen($attributes);
		if($original == $removed){
			$this->deleteCustomAttribute();
		}

		$session->set('attributes',$attributes);
		$attributes = $session->get('attributes');
		die("1");
		//$app = JFactory::getApplication();
		//$app->redirect("index.php?option=com_mica&view=seishowresults&Itemid=188");exit;
	}
	*/
public function deleteattribute(){
		$session    = JFactory::getSession();
		$removeattr = $this->input->get('attr', '', 'raw');

//////////////new code
		$chkcls = $this->input->get('chkcls', '', 'raw');
		$attributes = $session->get('attributes');

		if($chkcls=="swcs_checkbox")
		{
			///for composite score
			$removeattrs[] = "rural_score_".str_replace(" ","_",strtolower($removeattr));
			$removeattrs[] = "urban_score_".str_replace(" ","_",strtolower($removeattr));
			$removeattrs[] = "total_score_".str_replace(" ","_",strtolower($removeattr));



		}
		else if($chkcls=="mpi_checkbox")
		{
			///for market potential index
			//$original   = strlen($attributes);

		}
		else
		{
			/// for variables
			$removeattrs[] = strtolower($removeattr);
		}

////////////////////..end new code

// 		$attributes = $session->get('attributes');
// 		echo "<pre>";
// print_r($attributes);
//echo "</pre>";
		// $removeattrs[] = "rural_".str_replace(" ","_",strtolower($removeattr));
		// $removeattrs[] = "urban_".str_replace(" ","_",strtolower($removeattr));
		// $removeattrs[] = "total_".str_replace(" ","_",strtolower($removeattr));
		// $removeattrs[] = str_replace(" ","_",strtolower($removeattr));
		//$newclass    = str_replace(" ","_",$newclass);



		$original   = strlen($attributes);
		//echo "<pre/>";print_r($original);exit;

		// foreach($removeattrs as $removeattr){
		// 	$attributes = str_ireplace($removeattr, "", $attributes);
		// }


		 $attributes = explode(",", $attributes);


		 $attributes = array_udiff($attributes, $removeattrs,'strcasecmp');

		 $attributes =implode(",",$attributes);

		// //$attributes = array_filter($attributes);
		// $attributes = implode(",", $attributes);

		$removed = strlen($attributes);


		 if($original == $removed){
		 	$this->deleteCustomAttribute();
		 }

		$session->set('attributes',$attributes);
		$attributes = $session->get('attributes');
		//$app = JFactory::getApplication();

		die(json_encode($attributes));
		//$app->redirect("index.php?option=com_mica&view=seishowresults&Itemid=188");exit;
	}

	/**
	 * an ajax task.
	 */
	public function getsmeter(){
		$session = JFactory::getSession();
		$db      = JFactory::getDBO();

		$region      = urldecode( $this->input->get("region", '', 'raw'));
		$filterspeed = $this->input->get("filterspeed", '', 'raw');
		$speedvar    = urldecode( $this->input->get("speedvar", '', 'raw'));

		$activetable        = $session->get("activetable");
		$activenamevariable = $session->get("activenamevariable");
		$district = $session->get("district");
		$speedvar           = explode(",",$speedvar);

		/*foreach($speedvar as $eachvar){
			$maxspeedvar[] ="(select max(".$eachvar.") from ".$activetable.") as max".$eachvar;
			$minspeedvar[] ="(select min(".$eachvar.") from ".$activetable.") as min".$eachvar;
		}
		$maxspeedvar = implode(",",$maxspeedvar);
		$minspeedvar = implode(",",$minspeedvar);
		$speedvar    = implode(",",$speedvar);*/
		//$districts = explode(",", $district);
		/*if($_SERVER['REMOTE_ADDR'] == '122.170.14.145')
		{
			//echo $maxquery;
			echo "<pre>";
			print_r($maxresult);
		}*/

		//Edited rajesh 30-01-2013
		//For maximum value
		foreach($speedvar as $eachvar){
			
			$maxquery  = "SELECT ".$eachvar." FROM ".$activetable." WHERE ".$eachvar." IS NOT NULL AND ".$eachvar.">0 AND `state` != 'null' ORDER BY ".$eachvar."+0 DESC LIMIT 1";
			$db->setQuery($maxquery);
			$maxresult = $db->loadResult();
			if(empty($maxresult)){
				$maxresult = 0;
			}

			$maxspeedvar[] = $maxresult ." as max".$eachvar;
			//echo $db->getQuery();

			$minquery  = "SELECT ".$eachvar." FROM ".$activetable." where ".$eachvar." IS NOT NULL AND ".$eachvar.">0  ORDER BY ".$eachvar."+0 ASC LIMIT 1";

			$db->setQuery($minquery);
			$minresult = $db->loadResult();
			if(empty($minresult))	{
				$minresult = 0;
			}
			//echo "==>".$db->getQuery();	echo '<br/>';
			$minspeedvar[] = $minresult." as min".$eachvar;
		}
		$maxspeedvar = implode(",",$maxspeedvar);
		$minspeedvar = implode(",",$minspeedvar);
		$speedvar    = implode(",",$speedvar);

		//Edited ends

		//echo count(explode(",",$region));

		if(count(explode(",",$region))==1 && $filterspeed==0)		
		{

			//$region    =explode("-",$region);
			//$region    =$region[0];
			$temp_region = $region;
			$dash_pos    = strrpos($region, "-");
			if ($dash_pos === false) { // note: three equal signs
				// not found...
			}else{
				$region = substr($region,0,$dash_pos);
			}

			if($region == 'Diu Daman') {
				$region = 'Diu+Daman';
			}


			$state =  substr($temp_region,$dash_pos+1);
			$query = "SELECT ".$activenamevariable." as name,".$speedvar.",".$maxspeedvar.",".$minspeedvar."
				FROM ".$activetable."
				WHERE ".$activenamevariable." LIKE '".$region."'
					AND ".$db->quoteName('state')." LIKE '".$state."'";//group by ".$activenamevariable;


		}else{
		//print_r($region);
			$regionss    = explode(",",$region);
			$state_array = array();
			foreach($regionss as $eachregions){

				$temp_region = $eachregions;
				$dash_pos    = strrpos($eachregions, "-");
				if ($dash_pos === false) { // note: three equal signs
					// not found...
				}else{
				  $new_region =  substr($eachregions,0,$dash_pos);
				}

				if($new_region == 'Diu Daman') {
					$new_region = 'Diu+Daman';
				}
				$state_array []=  substr($temp_region,$dash_pos+1);


				/*$rname=explode("-",$eachregions);
				$tmprname = array();
				for($j=0;$j<count($rname);$j++){
					$tmprname[]=$rname[$j];
				}
				*/

				$sapregion[]=$new_region;
			}

			$regions = implode(",",$sapregion);
			$region  = str_replace(",","','",$regions);


			$query   = "SELECT ".$activenamevariable." as name,".$speedvar." ,";
			//$query .=" (select max(".$speedvar.") from ".$activetable.") as max".$speedvar." ,(select min(".$speedvar.") from ".$activetable.") as min".$speedvar."";
			$query   .= $maxspeedvar.",".$minspeedvar." ";
			$query   .= " , state FROM ".$activetable." WHERE ".$activenamevariable." IN ('".$region."') ";//  group by ".$activenamevariable;

		}

		$db->setQuery($query);
		//echo $activenamevariable;echo '<br>==<br/>';echo '<br>==<br/>'.$db->getQuery();
		$list = $db->loadAssocList();
		/*if($_SERVER['REMOTE_ADDR'] == '122.170.14.145'){
			
			print "<pre>";
			print_r($list);	
		}*/



		//echo $region;
		if(count(explode(",",$region))==1 && $filterspeed==0){
			$indexkey=0;
			foreach($list[0] as $key=>$val){

				if(isset($list[0]["min".$key])){
						/*echo '<br/>state:=>';echo $key;
						echo "<br/>key=>".$list[0][$key];
						echo "<br/>max=>".$list[0]["max".$key];
						echo "<br/>min=>".$list[0]["min".$key];
						echo '<br>==<br/>';*/
					if($list[0]["min".$key]>0){
						$per=($list[0][$key]*100)/($list[0]["max".$key]-$list[0]["min".$key]);
						$per=round($per);
						//$per="100.713331434";
						/*if($_SERVER['REMOTE_ADDR'] == '122.170.14.145'){
							echo $per1;
							echo "<br />".$list[0][$key];

							echo "<br />".$per;
						}*/
					}else{
						$per = 0;
					}
					//$op[$key]=$per;

					$op[$indexkey]['name']=$key;
					$op[$indexkey]['data']=$per;
					$indexkey++;
				}

			}
		}else{
			//print '<pre>'; print_r($list);
			$indexkey=0;

			foreach($list as $eachlist){
				foreach($eachlist as $key=>$val){
					//echo $eachlist['state'];
					//echo '<br>==<br/>';					 var_dump(in_array($eachlist['state'],$state_array));
					if(isset($eachlist["min".$key])){
						/*echo '<br/>state:=>';echo $key;
						echo "<br/>key=>".$eachlist[$key];
						echo "<br/>max=>".$eachlist["max".$key];
						echo "<br/>min=>".$eachlist["min".$key];
						echo '<br>==<br/>';*/
						//$op[$indexkey]['name'] = $eachlist['name'].' - '.$newlabel." ".$sublabel;
						if(in_array($eachlist['state'],$state_array)){
							if($eachlist["min".$key]>0){
								$per = ($eachlist[$key]*100)/($eachlist["max".$key]-$eachlist["min".$key]);
								$per = round($per);
							}else{
								$per = 0;
							}
							$op[$indexkey]['name'] =$key;
							$op[$indexkey]['name'] =$eachlist['name'].' - '.$eachlist['state'].'--'.str_replace("_"," ",$key);
							$op[$indexkey]['data'] =$per;
							$indexkey++;
						}
					}
				}
			}
		}

		if(count($op)>0){
			$script="<script type='text/javascript'>
				function initspeed(){";
					$search_chars = array(" ", "&", "+");

					//foreach($op as $key=>$val)
					for($l=0;$l<count($op);$l++){
						$key = $op[$l]['name'];
						$val = $op[$l]['data'];
							$script.="	JQuery('#".$l.str_replace($search_chars,"_",$key)."').speedometer();
									JQuery('#".$l.str_replace($search_chars,"_",$key)."').speedometer({ percentage: ".$val." || 0 });";
					}

			$script.="	}
			</script>";
		}

		$div .="<table><tr>";
		//foreach($op as $key=>$val)
		if(count($op)>0){
			for ($l=0,$c=0;$l<count($op);$l++,$c++)
			{
				if($c==3)
				{
					$c=0;
					$div.="</tr><tr>";
				}
				$key = $op[$l]['name'];
				$val = $op[$l]['data'];
				//$div .='<td ><div class="speed_m_div" id="'.str_replace(" ","_",$key).'" style="float:right;">'.array($key=>$val).'</div></td>';
				$tmpjtext = array($key=>$val);
				//print_r($tmpjtext);
				$div .='<td style="text-align:center;" ><div class="potentio_data"><div class="potentio_meter" style="text-align:center;" class="speed_m_div" id="'.$l.str_replace($search_chars,"_",$key).'" style="float:right;">'.$val.'</div><br><div style="text-align:center;">'.JText::_($key).'</div> </div></td>';
			}
		}

		/*$div .="</tr><tr>";
		//foreach($op as $key=>$val){
		for($l=0;$l<count($op);$l++)
		{
			$key = $op[$l]['name'];
			$val = $op[$l]['data'];
			$div .='<td style="text-align:center;">'.JText::_($key).'</td>';
		}*/

		$div .="</tr></table>";


		echo $opstring=$javascriptstring.$script.$div;exit;
	}

	/**
	 * Ajax task.
	 */
	public function deleteCustomAttribute(){
		$removeattr = $this->input->get('attr', '', 'raw');
		$this->input->set('attrname', $removeattr);

		$this->deleteCustomAttr(1);
		$this->deletethematicQueryToSession(1);
		return true;
		/*$session = JFactory::getSession();
		$attributes=$session->get('customattribute');
		$attributes=explode(",",$attributes);
		foreach($attributes as $k=>$eachattr)
		{	if(strstr($eachattr,$removeattr))
			{
				unset($attributes[$k]);
			}

		}
		//echo "<pre>";print_r($attributes);
		$attributes=array_filter($attributes);
		$attributes=implode(",",$attributes);
		$session->set('customattribute',$attributes);*/
		//return true;
	}

	function deleteCustomVariableFromLib()
	{
		$session     = JFactory::getSession();
		$db          = JFactory::getDbo();

		$workspaceid = $session->get('activeworkspace');
		$activetable = $session->get('activetable');

		$name = $this->input->get("attrname", '', 'raw');
		$name = explode(",",$name);

		foreach($name as $eachvariable){
			$this->input->set('attr', $eachvariable);
			$this->deleteCustomAttribute();

			$query = "DELETE FROM ".$db->quoteName('#__mica_user_custom_attribute')."
				WHERE ".$db->quoteName('profile_id')." = ".$db->quote($workspaceid)."
					AND ".$db->quoteName('name')." LIKE ".$db->quote($eachvariable);
			$db->setQuery($query);
			$db->execute();

			$query = "DELETE FROM ".$db->quoteName('#__mica_user_custom_attribute_lib')."
				WHERE ".$db->quoteName('profile_id')." = ".$db->quote($workspaceid)."
					AND ".$db->quoteName('name')." LIKE ".$db->quote($eachvariable)."
					AND ".$db->quoteName('tables')." LIKE ".$db->quote($activetable);
			$db->setQuery($query);
			$db->execute();
		}
		$data = array("success" => 1);
		echo json_encode($data);
		die();
		//$app = JFactory::getApplication();
		//$app->redirect("index.php?option=com_mica&view=seishowresults&Itemid=188");
	}

	/**
	 * ajax task.
	 */
	public function deleteVariable(){
		$session = JFactory::getSession();

		$table = $this->input->get('table', '', 'raw');
		$value = $this->input->get('value', '', 'raw');

		if($table == "name"){

			$state      =$session->get('state');
			$attributes =str_replace($value,"",$state);
			$attributes =explode(",",$attributes);
			$attributes =array_filter($attributes);
			$attributes =implode(",",$attributes);
			$session->set('state',$attributes);

		}else if($table=="distshp"){

			$district   =$session->get('district');
			$attributes =str_replace($value,"",$district);
			$attributes =explode(",",$attributes);
			$attributes =array_filter($attributes);
			$attributes =implode(",",$attributes);
			$session->set('district',$attributes);

		}else if($table=="place_name"){

			$town       =$session->get('town');

			$attributes =str_replace($value,"",$town);
			$attributes =explode(",",$attributes);
			$attributes =array_filter($attributes);
			$attributes =implode(",",$attributes);
			$session->set('town',$attributes);

		}else if($table=="UA_Name"){

			$urban      =$session->get('urban');
			$attributes =str_replace($value,"",$urban);
			$attributes =explode(",",$attributes);
			$attributes =array_filter($attributes);
			$attributes =implode(",",$attributes);
			$session->set('urban',$attributes);
		}
		exit;
	}

	/**
	 * Internal function fetches URL.
	 */
	function getTomcatUrl(){
		$db    = JFactory::getDBO();
		$query = "SELECT tomcatpath FROM ".$db->quoteName('#__mica_configuration');
		$db->setQuery($query);
		$this->tomcat_url = $db->loadResult();
	}

	/**
	 * AJAX task.
	 */
	public function popupAttributes(){
		$session = JFactory::getSession();

		$attributes = $session->get('attributes');
		$town       = $session->get('town');
		$urban      = $session->get('urban');

		$id   = $this->input->get("id", '', 'raw');
		$id   = trim($id);
		$zoom = $this->input->get("zoom", '', 'raw');

		if($zoom ==5 || $zoom==6){

			$searchvariable  = $this->districtsearchvariable;
			//$district      = $session->get('district');
			//$session->set('district',$district.",".$id);
			$district_orgfid = $session->get('district_orgfid');
			$session->set('district_orgfid',$district_orgfid.",".$id);
			$sld = $this->sldPopup($searchvariable,$id, "india_information", "distshp");

		}else if($zoom==7){

			$searchvariable  = $this->districtsearchvariable;
			//$district      = $session->get('district');
			//$session->set('district',$district.",".$id);
			$district_orgfid = $session->get('district_orgfid');
			$session->set('district_orgfid',$district_orgfid.",".$id);
			$sld = $this->sldPopup($searchvariable,$id, "india_information", "distshp");

		}else  if($zoom==8){

			$searchvariable  = $this->districtsearchvariable;
			//$district      = $session->get('district');
			//$session->set('district',$district.",".$id);
			$district_orgfid = $session->get('district_orgfid');
			$session->set('district_orgfid',$district_orgfid.",".$id);
			$sld = $this->sldPopup($searchvariable,$id, "india_information", "distshp");

		}else{

			$searchvariable  = $this->districtsearchvariable;
			//$district      = $session->get('district');
			//$session->set('district',$district.",".$id);
			$district_orgfid = $session->get('district_orgfid');
			$session->set('district_orgfid',$district_orgfid.",".$id);
			$sld = $this->sldPopup($searchvariable,$id, "india_information", "distshp");

		}

		$session->set('state_orgfid',null);
		$session->set('district_orgfid',null);
		$session->set('town_orgfid',null);
		$session->set('urban_orgfid',null);

		$str    = "<div ><table class='popupmaintable ' cellspacing='0' cellpadding='0' border='1'>";
		$header = "";
		$sld    = array_reverse($sld);

		if(count($sld)>0){

			$header .= "<table class='popupheader'><tr>";
			$header .="<td class='popupleft'>Name</td>";
			$header .="<td class='popupright'>".$sld[0]["text"]."</td>";
			$header .= "</tr></table>";

			foreach($sld as $eachsld){
				if($eachsld['custom_name'] == ""){
					$name = $eachsld['custom_formula'];
				}else{
					$name = $eachsld['custom_name'];
				}

				$str .= "<tr >";
				$str .="<td >".JTEXT::_($name)."</td>";//custom_value
				$str .="<td >".$eachsld['custom_value']."</td>";

				if($eachsld['level'] != 0){
					$color= str_replace("#","",$eachsld['color']);
					$str .="<td ><img src='".JURI::base()."components/com_mica/maps/img/layer".$eachsld['level']."/pin".$color.".png' /></td>";
				}else{
					$str .="<td style='background:".$eachsld['color'].";'></td>";
				}
				$str .= "</tr>";
			}

		}else{

			$model = $this->getModel();//'seishowresults','seishowresultsModel'
			$data  = $model->getCustomData(1,1);
			//echo $searchvariable;exit;
			$header       = "";
			foreach($data as $basekey => $object){
				foreach($object as $key => $val){
					//echo $object->$searchvariable."->".$id."<br>";
					if( $object->$searchvariable == $id && $key != $searchvariable && $key!="state" && $key!="distshp"){
						if(strstr($key,"name")){

							$header .= "<table class='popupheader'><tr>";
							$header .="<td class='popupleft'>".JTEXT::_($key)."</td>";
							$header .="<td class='popupright'>".$val."</td>";
							$header .= "</tr></table>";

						}else{

							$str .= "<tr >";
							$str .="<td class='popupleft'>".JTEXT::_($key)."</td>";
							$str .="<td class='popupright'>".$val."</td>";
							$str .= "</tr>";

						}
					}
				}
			}
		}
		$str .= "</table></div>";

		echo $header.$str;exit;
		//echo $id;//echo $attributes;exit;
	}


	/**
	 *
	 */
	function sldPopup($searchvarable, $searchvalue, $basetable, $searchtextvariable){
		$session = JFactory::getSession();
		$db      = JFactory::getDBO();

		$activeworkspace = $session->get('activeworkspace');
		$finalarray      = array();

		$query = "SELECT sld.*, sld.layerfill, t1.".$searchtextvariable." , t2.*
			FROM ".$db->quoteName('#__mica_map_sld')." AS sld ,
				".$db->quoteName($basetable)." AS t1 ,
				".$db->quoteName('#__mica_sld_legend')." AS t2
			WHERE ".$db->quoteName('sld.text')." = ".$db->quoteName("t1.".$searchtextvariable)."
				AND ".$db->quoteName("t1.".$searchvarable)." = ".$db->quote($searchvalue)."
				AND ".$db->quoteName('profile_id')." = ".$db->quote($activeworkspace)."
				AND ".$db->quoteName('t2.workspace_id')." = ".$db->quoteName('profile_id')."
				AND ".$db->quoteName('sld.layerfill')." = ".$db->quoteName('t2.color')."
			GROUP BY ".$db->quoteName('t2.level')." DESC ";
		$db->setQuery($query);
		$list = $db->loadAssocList();

		foreach($list as $eacharray){
			$query = "SELECT name
				FROM ".$db->quoteName('#__mica_user_custom_attribute')."
				WHERE ".$db->quoteName('attribute')." = ".$db->quote($eacharray['custom_formula'])."
					AND ".$db->quoteName('profile_id')." = ".$db->quote($eacharray['profile_id']);
			$db->setQuery($query);
			$customformulaname = $db->loadResult();

			if($customformulaname == ''){
				$customname = array("custom_name"=>"");
			}else{
				$customname = array("custom_name"=>$customformulaname);
			}

			$eacharray = array_merge($customname,$eacharray);

			$query = "SELECT (".$eacharray['custom_formula'].") as customval FROM ".$basetable." WHERE ".$searchvarable."=".$searchvalue;
			$db->setQuery($query);
			$customformulavalue = $db->loadResult();

			$customvalue  = array("custom_value"=>$customformulavalue);
			$eacharray    = array_merge($customvalue,$eacharray);
			$finalarray[] = $eacharray;
		}
		return $finalarray;
	}

	/**
	 * A task to fetch attributes from AJAX call.
	 */
	public function getAttribute(){
		$zoom = $this->input->get('zoom', '', 'raw');
		$type = $this->input->get('fileId', '', 'raw');

		$user = JFactory::getUser();
		if($user->id <= 0){
			echo -1;exit;//JRoute::_('index.php?option=com_user&view=login');
		}
		echo $values = $this->getPlanAttr($zoom,$type);

		exit;
	}

	/**
	 *
	 */
	function buildTree(array $elements, $parentId = '') {
	    $branch = array();
	    foreach ($elements as $element) {
	    	if ($element->variable_parent != '') {
	    		if ($element->variable_parent == $parentId) {
		            $branch[] = $element->variable_name;
		        }
	    	}
	    }
	    return $branch;
	}


	function getPlanAttr($zoom, $type = null){

		$db = JFactory::getDBO();
		$fileId = $type;
		if ($fileId) {
			$query = "SELECT vr.id, coalesce(dm.name, vr.variable_name) as custom_name FROM `variable_rows` as vr left join data_mapping as dm on vr.id = dm.variable_row_id WHERE vr.variable_type LIKE 'variable' and vr.dataset_master_id = '".$fileId."' and vr.is_filterable = '1' group by custom_name order by vr.id asc";
			$db->setQuery($query);
			$variable_list = $db->loadAssocList();

			$query = "SELECT vc.* FROM `variable_columns` as vc join variable_rows as vr on vc.group_id = vr.id WHERE vc.group_id = vr.id and vr.variable_type LIKE 'variable' and vr.dataset_master_id = '".$fileId."' and vr.is_filterable = '1' order by vc.variable_group_title asc ";
			// die($query);
			$db->setQuery($query);
			$variable_cols_data = $db->loadObjectList();
			$vcData = [];
			foreach ($variable_cols_data as $key => $value) {
				$vcData[$value->variable_group_title][] = $value->variable_name;
			}

			$query = "SELECT * FROM `variable_rows`
						WHERE variable_type LIKE 'variable' 
	       				AND dataset_master_id = '".$fileId."' 
	   					AND is_filterable = '1' 
	       				AND id NOT IN 
	       				(SELECT vc.group_id 
	       					FROM `variable_columns` AS vc 
	                      	JOIN variable_rows AS vr 
	                      	ON vc.group_id = vr.id 
	                      	WHERE  vc.group_id = vr.id 
	                      	AND vr.variable_type LIKE 'variable' 
	                      	AND vr.dataset_master_id = '".$fileId."' 
	                      	AND vr.is_filterable = '1' 
	                      	ORDER  BY vc.variable_group_title ASC)
	                    AND id not in (SELECT variable_row_id FROM `data_mapping` WHERE `dataset_master_id` = '".$fileId."' )";
			$db->setQuery($query);
			$variable_rows_data = $db->loadObjectList();
			$vrData = [];
			$vrData_child = [];
			foreach ($variable_rows_data as $key => $value) {
				if ($value->variable_parent == '') {
					$vrChildData = $this->buildTree($variable_rows_data, $value->id);
					sort($vrChildData);
					$vrData[$value->variable_name] = $vrChildData;
				}
			}

			$query = "SELECT * FROM `data_mapping` WHERE `dataset_master_id` = '".$fileId."' group by name order by name ";
			$db->setQuery($query);
			$mapping_variable_data = $db->loadObjectList();
			$vmData = [];
			foreach ($mapping_variable_data as $key => $value) {
				$vmData['Common'][] = $value->name;
			}

			$vData_half = array_merge($vrData, $vcData, $vmData);
			$vData = [];
			foreach ($vData_half as $key => $value) {
				if (count($value) > 0) {
					$vData[$key] = $value;
				} else {
					$vData['Custom'][] = $key;
				}
			}
			ksort($vData);
			$vx = 0;
			$custom_ret .= '<ul class="list1 variablelist">';
			foreach ($vData as $key => $value) {
				$string = preg_replace('/\s+/', '_', strtolower($key));
				$custom_ret .= '<li class="variable_group" id="variable_group_'.$string.'"><label class="variable_label">'.$key.'</label><div class="fright"><input type="checkbox" class="variableNameGrp allcheck variablelabel_checkbox-'.$string.'" id="variablelabel_allcheck-'.$string.'"><label>&nbsp;</label></div></li>';
				foreach ($value as $k => $v) {
					$class = strtolower(str_replace(" ", "_", $key));
					$custom_ret .= '<li class="inner_value '.$class.'"><input type="checkbox" class="variable_checkbox variablelabel_checkbox-'.$string.'" name="attributes[]" data-category = "variable_check_'.$vx.'" value="'.$v.'" id="variable_check_'.$vx.'">
							<label for="variable_check_'.$vx.'" id="test">'.JTEXT::_($v).'</label></li>';
					$vx++;
				}
			}
			$custom_ret .= "</ul>";
			return $custom_ret;
		}

		$prefix = 0;
		if($type==null){
			if($zoom == 5){
				$db_table = "State";
				$prefix   = 1;
			}else if($zoom > 5 && $zoom <= 7){
				$db_table = "District";
				$prefix   = 1;
			}else{
				$db_table = "Town";
			}
		}else{
			$db_table = "district";
		}

		$userAttr = $this->userSelectedAttr($db_table);

		if($db_table == "state"){
			$prefix = 1;
		}else if($db_table == "district"){
			$prefix = 1;
		}else{
			$prefix = 0;
		}

		if($userAttr == -2){
			return -2;
		}

		$str       = "";
		$todisplay = array();
		$db        = JFactory::getDBO();
		//edited by date on 21/01/2013
			//$query="select * from #__mica_group_field allfields JOIN #__mica_group grp ON grp.id=allfields.groupid where allfields.table like '".$db_table."' and grp.publish =1 order by grp.group,allfields.field ASC";
		$query = "SELECT * FROM ".$db->quoteName('#__mica_group_field')." AS allfields
			INNER JOIN ".$db->quoteName('#__mica_group')." AS grp ON ".$db->quoteName('grp.id')." = ".$db->quoteName('allfields.groupid')."
			WHERE ".$db->quoteName('allfields.table')." LIKE ".$db->quote($db_table)."
				AND ".$db->quoteName('grp.publish')." = ".$db->quote(1)."
			ORDER BY ".$db->quoteName('grp.ordering').", ".$db->quoteName('allfields.field')." ASC";
		$db->setQuery($query);
		$list = $db->loadAssocList();

		foreach($list as $each){
			if($each['group'] != "Mkt Potential Index" && $each['group'] != "Score"){
				if($prefix == 1){
					$each['field'] = str_replace($this->ruralprefix,"",$each['field']);
					$each['field'] = str_replace($this->urnbanprefix,"",$each['field']);
					$each['field'] = str_replace($this->totalprefix,"",$each['field']);
					$todisplay[$each['group']][] = $each['field'];
				}else{
					$todisplay[$each['group']][] = $each['field'];
				}
				$icon[$each['group']] = $each['icon'];
			}else{
				if($prefix == 1){
					$each['field'] = str_replace($this->ruralprefix,"",$each['field']);
					$each['field'] = str_replace($this->urnbanprefix,"",$each['field']);
					$each['field'] = str_replace($this->totalprefix,"",$each['field']);
					$todisplay1[$each['group']][] = $each['field'];
				}else{
					$todisplay1[$each['group']][] = $each['field'];
				}
				$icon[$each['group']] = $each['icon'];
			}
		}

		$todisplay = $todisplay;

		foreach($todisplay as $eachgrp=>$vals){
			$todisplay[$eachgrp] = array_unique($todisplay[$eachgrp]);
		}

		foreach($userAttr as $each){
			if($prefix == 1){
				$each = str_replace($this->ruralprefix,"",$each);
				$each = str_replace($this->urnbanprefix,"",$each);
				$each = str_replace($this->totalprefix,"",$each);
				$userAttr1[] = $each;
			}else{
				$userAttr1[] = $each;
			}

		}

		$userAttr = array_unique($userAttr1);
		$session  = JFactory::getSession();
		$oldattr  = $session->get('attributes');


		$urban   = "";
		$town    = array();
		$other   = "";
		$str     = "";
		$i       = 0;
		$str1    = "";
		$other   = array();
		$urban   = array();
		$total   = array();
		$grpname = array();
		$header  = "";
		$div     = "";
		$returndata = "";

/////////////////////////////////Custom Variables
			//$model   = $this->getModel();

		$shortcode.='<a class="addcustomvariable" href="javascript:void(0)" onclick="document.getElementById(\'light\').style.display=\'block\';" id="custom_btn">
										<i class="fa fa-plus-circle"></i>Custom</a>';


			 $model        = $this->getModel();
			 $district = $this->input->get('district');

			if($district !='')
			{

				$data         = $model->getCustomData(1,1,true,$district);
				$model->getcustomattrlib();
				$custolibmarray=explode(",",$session->get('customattributelib'));
				$customattribute = $session->get('customattribute');
		        $customarray    = explode(",",$customattribute);
		        $finalarray     = array(array_filter($customarray), array_filter($custolibmarray));

		        $ret1.='<ul class="list1">';
			  	/*$ret1 .='<li class="customvariable_group poptitle"><label class="poptitle">Custom Variable</label>
			  	   </li>';*/

	            $i             = 0;
				$finalarray[1] = array_unique($finalarray[1]);
				foreach($finalarray[1] as $eacharray){
					if(in_array($eacharray, $finalarray[0])){
						$checked  = "checked";
						$selected = "Remove";
						$action   = "";
					}else{
						$checked  = "";
						$selected = "Select";
						$action   = "checked";
					}

					if(($i%2)==0){
						$clear="clear";
					}else{
						$clear="";
					}
					$namevalue=explode(":",$eacharray);


					$ret1.='<li><input type="checkbox"  name="custom_variable['.$namevalue[0].']" '.$checked.' class="customvariablecheckbox" value="'.$namevalue[1].'" id="'.$namevalue[0].'">
						<label for="'.$namevalue[0].'">'.JTEXT::_($namevalue[0]).'</label></li>';

				}

			/*	if(!empty($finalarray[1])) {*/

						 //}
				$ret1.="</ul>";

				/*include  JPATH_ROOT.'/templates/micamimi/html/com_mica/micafront/custom_variable.php';*/

		    }



///////////////////////////////// custom variables end


		if($this->input->get('grplist', '', 'raw') == ""){

			$x=0;
			$ret.='<ul class="list1 variablelist">';
			$shortcode.='<ul class="var_ul" >';
			foreach($todisplay as $keys=>$district_data){


				 $abc=strtolower($keys);
				 $string = preg_replace('/\s+/', '_', $abc);
				$ret .='<li class="variable_group" id="variable_group_'.$string.'"><label class="variable_label">'.$keys.'</label><div class="fright"><input type="checkbox" class="variableNameGrp allcheck variablelabel_checkbox-'.$string.'" id="variablelabel_allcheck-'.$string.'"><label>&nbsp;</label></div></li>';
				$shortcode.='<li><span class="tooltip-txt-box"><a class="'.$string.'"  href="#variable_group_'.$string.'">'.$keys.'</a><span class="tooltiptext">'.$keys.'</span></span></div></li>';
				foreach($district_data as $key=>$eachattr){



					if($eachattr=="Rating" ||$eachattr=="MPI"){$checked="selected";$checked1="checked";}
					else if(stristr($oldattr,$eachattr)){$checked="selected";$checked1="checked";}else{ $checked="";$checked1="";}
					$class=strtolower(str_replace(" ", "_", $keys));


					$ret.='<li class="inner_value '.$class.'"><input type="checkbox" class="variable_checkbox variablelabel_checkbox-'.$string.'" name="attributes[]" data-category = "variable_check_'.$x.'" value="'.$eachattr.'" '.$checked1.' id="variable_check_'.$x.'">
						<label for="variable_check_'.$x.'" id="test">'.JTEXT::_($eachattr).'</label></li>';
						$x++;
				}

			}
			$shortcode.='</ul>';



			$ret.="</ul>";



				return $ret.'split'.$shortcode.'split'.$ret1 ;//$return;

		}else{

			$grplist = $this->input->get('grplist', '', 'raw');
			$grplist = explode(",",$grplist);

			$returndata .= '<select name="attributes[]" id="attributes" class="inputbox"   multiple="multiple" >';

			foreach($todisplay as $key => $district_data){
				if(in_array($key,$grplist)){
					$returndata .= '<optgroup label="'.$key.'">';
					foreach($district_data as $key=>$eachattr){
						if($eachattr=="Rating" ||$eachattr=="MPI"){$checked="selected";}
						else if(stristr($oldattr,$eachattr)){$checked="selected";}else{ $checked="";}

						// edited by dave 21/01/2013
						//$returndata .= '<option value="'.$eachattr.'" '.$checked.'>'.JTEXT::_($eachattr).'</option>';
						$returndata .= '<option value="'.$eachattr.'" '.$checked.'>'.JTEXT::_($eachattr).'</option>';
					}
					$returndata .= '</optgroup >';
				}
			}
			$returndata .= '</select>';
			return $returndata;
		}

		foreach($todisplay as $key => $val){
			$grpname[] = $key;
			$str[$i]   = "";
			if($icon[$grpname[$i]]!=""){
				$img=$icon[$grpname[$i]];
			}else{
				$img="default.png";
			}

			$header .="<li ><a href='#".str_replace(" ","_",trim($grpname[$i]))."grp'><div class='grpimg'><img src='".JUri::base()."components/com_mica/images/".$img."'/></div><div class='grptitle'>".$grpname[$i]."</div></a></li>";
			//$oldattr=explode(",",$oldattr);

			foreach($userAttr as $eachattr){
				//echo $eachattr."<br>";
				if(in_array($eachattr,$val)){
					if($eachattr=="Rating" ||$eachattr=="MPI"){$checked="checked";}
					else if(stristr($oldattr,$eachattr)){$checked="checked";}else{ $checked="";}

						$other[$i][]='
					<div class="variablechkbox ">
					<input type="checkbox" name="attributes[]" class="statetotal_attributes hideurban '.str_replace(" ","_",$key).'" value="'.$eachattr.'" '.$checked.'>
					</div>
					<div class="variabletext hideurban"><span class="hovertext '.JTEXT::_(trim($eachattr)).'">'.ucfirst(strtolower($eachattr)).'</span></div>';
				}
			}
			$i++;
		}

		$str1 .='<div class="maintableattr">
			<div class="left">
			<a class="statetotalselectall" href="javascript:void(0);" onclick="checkAll(statetotal_attributes);">'. JText::_('SELECT_ALL_LABEL').'</a> / <a class="statetotalunselectall" onclick="uncheckall(statetotal_attributes);" href="javascript:void(0);">'.JText::_("UNSELECT_ALL_LABEL").'</a>
			</div></div>';

		$finalstr   ="";
		$town1      ="";
		$urban1     ="";
		$others1    ="";
		$mystrarray =array();
		$total1     =array();

		for($i=0;$i<count($str);$i++){
			$finalstr ="";
			$town1    ="";
			$urban1   ="";
			$others1  ="";
			$total1   ="";

			for($j=0;$j<count($other[$i]);$j++){
				if(($j%3)==0){
					$others1 .="</div><div class='maintableattr'>";
				}
				$others1 .=$other[$i][$j];
			}
			$othersss ="<div id='".str_replace(" ","_",trim($grpname[$i]))."grp' class='singlegrp'>";

			$others1    =$othersss."<div>".$others1;
			//$finalstr .=$str[$i];
			//$finalstr .="</div><div class='maintableattr ' >".$urban1."";
			//$finalstr .="</div><div class='maintableattr '>".$total1."</div>";
			//$others1  .="</div>";
			$finalstr   .=$others1."";

			$finalstr .='<div class="maintableattr">
				<div class="selectallfront">
				<a class="statetotalselectall" href="javascript:void(0);" onclick="checkAll(\''.str_replace(" ","_",trim($grpname[$i])).'\');">'. JText::_('SELECT_ALL_LABEL').'</a> / <a class="statetotalunselectall" onclick="uncheckall(\''.str_replace(" ","_",trim($grpname[$i])).'\');" href="javascript:void(0);">'.JText::_("UNSELECT_ALL_LABEL").'</a>
				</div></div></div></div>';
			$mystrarray[$i]=$finalstr;
		}

		return "<div id='tabs'><ul>".$header."</ul>".implode("",$mystrarray)."</div></div></div>";
		//return $str;
	}

	/**
	 *
	 */
	function userSelectedAttr($db_table){
		$db = JFactory::getDBO();
		$query = " SELECT plan_id
			FROM ".$db->quoteName('#__osmembership_subscribers')." AS a
			WHERE ".$db->quoteName('plan_subscription_status')." =  ".$db->quote(1)."
				AND ".$db->quoteName('user_id')." = ".$db->quote(JFactory::getUser()->id);
		$db->setQuery($query);
		$plan_id = (int) $db->loadResult();

		$query = "SELECT attribute FROM ".$db->quoteName("#__mica_user_attribute")."
			WHERE ".$db->quoteName('aec_plan_id')." = ".$db->quote($plan_id)."
				AND ".$db->quoteName('dbtable')." LIKE ".$db->quote($db_table);
		$db->setQuery($query);
		$row = $db->loadResult();

		$attr = explode(",",$row);
		return $attr;
	}

	/**
	 * ajax task.
	 */
	public function AddCustomAttr(){
		$db      = JFactory::getDBO();
		$user    = JFactory::getUser();
		$session = JFactory::getSession();

		if($user->id == 0){
			return -1;
		}

		$attributevale   = $this->input->get('attributevale', '', 'raw');
		$attrname        = $this->input->get('attrname', '', 'raw');
		$customattribute = $session->get('customattribute');
		$attributevale   = $attrname.":".$attributevale.",".$customattribute;

		// Set Custom variable in session
		$session->set('customattribute',$attributevale);

		// Update Custom data in workspace
		workspaceHelper::updateWorkspace();

		exit;
	}

	/**
	 * ajax task.
	 */
	public function getCustomAttr(){
		$db      = JFactory::getDBO();
		$user    = JFactory::getUser();
		$session = JFactory::getSession();
		if($user->id == 0){
			return -1;
		}

		$query = "SELECT count(id) FROM ".$db->quoteName('#__user_profile')." WHERE ".$db->quoteName('user_id')." = ".$db->quote($user->id);
		$db->setQuery($query);
		$count = $db->loadResult();
		if($count == 0){
			return -2;
		}

		$query = "SELECT id FROM ".$db->quoteName('#__user_profile')." WHERE ".$db->quoteName('user_id')." = ".$db->quote($user->id);
		$db->setQuery($query);
		$row = $db->loadResult();

		$query = "SELECT * FROM ".$db->quoteName('#__user_custom_attribute')." WHERE ".$db->quoteName('profile_id')." = ".$db->quote($row);
		$db->setQuery($query);
		$attr = $db->loadAssocList();

		$str = "";
		foreach($attr as $eachattr){
			$str .= '
			<tr >
				<td align="left" valign="top"><input type="checkbox" name="customattributes[]" class="customtotal_attributes" id="d'.$eachattr['id'].'" value="'.$eachattr['attribute'].'" checked="checked"></td>
				<td align="left" valign="top" class="d'.$eachattr['id'].'">'.$eachattr['name'].'</td>
				<td id="del_custom_'.$eachattr['id'].'" class="delcustom"> X </td>
			</tr>';
		}
		echo $str;exit;
	}

	/**
	 * An ajax task
	 */
	public function updateCustomAttr(){
		$session = JFactory::getSession();

		$attributevale = $this->input->get('attributevale', '', 'raw');
		$oldattrval    = $this->input->get('oldattrval', '', 'raw');
		$attrname      = $this->input->get('attrname', '', 'raw');

		$attributes = $session->get('customattribute');
		$attributes = str_replace($oldattrval, $attributevale, $attributes);
		$session->set('customattribute',$attributes);

		$activeworkspace          = $session->get('activeworkspace');
		$customformulafromsession = $session->get('customformula');
		$customformulafromsession = str_replace($oldattrval,$attributevale,$customformulafromsession);
		$session->set('customformula',$customformulafromsession);

		workspaceHelper::updateWorkspace();
		exit;
	}

	/**
	 * An ajax task
	 */
	public function deleteCustomAttr($innercall = null){
		/*$db = JFactory::getDBO();
		$user = JFactory::getUser();
		$id=$this->input->get("id", '', 'raw');
		if($user->id == 0){
			return -1;
		}
		$query="DELETE FROM ".$this->_table_prefix."user_custom_attribute WHERE id = ".$id;
		$db->setQuery($query);
		$db->query();
		echo 1;*/

		$session = JFactory::getSession();

		$attrname   = $this->input->get('attrname', '', 'raw');
		$attributes = $session->get('customattribute');
		$attributes = explode(",",$attributes);
		$val        = array();
		foreach($attributes as $k =>$eachattr){
			$nameandattr = explode(":",$eachattr);
			if($nameandattr[0] == "" || $nameandattr[0] == $attrname){
				unset($attributes[$k]);
				$val[] = $nameandattr[1];
			}

		}

		$attributes = array_filter($attributes);
		$attributes = implode(",", $attributes);

		$session->set('customattribute',$attributes);
		$customformulafromsession = $session->get('customformula');
		$geteachformula           = explode("|",$customformulafromsession);
		foreach($geteachformula as $key => $eachtocheck){
			$singleformula = explode(":",$eachtocheck);
			if(in_array($singleformula[0],$val)){
				unset($geteachformula[$key]);
				$segment = explode("->",$singleformula[1]);

				$this->input->set('level',end($segment));
			}
		}

		$customformulafromsession = implode("|",$geteachformula);
		$session->set('customformula', $customformulafromsession);
		//echo $customformulafromsession;exit;
		workspaceHelper::updateWorkspace();
		if($innercall != null){
			return true;
		}else{
			exit;
		}
	}

	/**
	 * a task for page.
	 */
	public function loadWorkspace($msg = null){

		$app    = JFactory::getApplication();
		$result = workspaceHelper::loadWorkspace();


		$user   = JFactory::getUser();
		if($user->id == 0){
			return -1;
		}

		if(!empty($result) && count($result) > 0){
			$session = JFactory::getSession();
			$db      = JFactory::getDBO();

			$restore = unserialize($result[0]['data']);

			$session->set('attributes', $restore['attribute']);
			$session->set('m_type', $restore['m_type']);
			$session->set('state', $restore['state']);
			$session->set('district', $restore['district']);
			$session->set('urban', $restore['urban']);
			$session->set('town', $restore['town']);
			$session->set('is_default', $result[0]['is_default']);

			$workspaceid = $this->input->get("workspaceid", '', 'raw');
			$session->set('gusetprofile','');
			$session->set('activeworkspace',$workspaceid);

			$query = "SELECT count(*)
				FROM ".$db->quoteName('#__mica_user_custom_attribute_lib')."
				WHERE ".$db->quoteName('profile_id')." = ".$db->quote($workspaceid);
			$db->setQuery($query);
			$count = $db->loadResult();

			if($count != 0){

				$query = " SELECT name, attribute
					FROM ".$db->quoteName("#__mica_user_custom_attribute")."
					WHERE ".$db->quoteName('profile_id')." = ".$db->quote($workspaceid);
				$db->setQuery($query);
				$allcostomattr = $db->loadAssocList();

				$str = "";
				foreach($allcostomattr as $eachcustomattr){
					$eachcustomattr['attribute'] = str_replace("'", " ", $eachcustomattr['attribute']);
					$str .= $eachcustomattr['name'].":".$eachcustomattr['attribute'].",";
				}

				$attributes = $session->set('customattribute',$str);

				$query = "SELECT name, attribute
					FROM ".$db->quoteName('#__mica_user_custom_attribute_lib')."
					WHERE ".$db->quoteName('profile_id')." = ".$db->quote($session->get('activeworkspace'));
				$db->setQuery($query);
				$allcostomattr = $db->loadAssocList();

				$str = "";
				foreach($allcostomattr as $eachcustomattr){
					$eachcustomattr['attribute'] = str_replace("'", " ", $eachcustomattr['attribute']);
					$str .= $eachcustomattr['name'].":".$eachcustomattr['attribute'].",";
				}

				$session->set('customattributelib', $str);
				$session->set('customattribute', $str);
			}
			else
			{
				$session->set('customattribute', null);
				$session->set('customattributelib', null);
			}
		}

		if($msg == "insert"){
			$msg = "&msg=0";
			$this->saveSldToDataBase("1");
			$session->set('customformula', null);
			//$session->set('customformula',null);
			//$this->saveSldToDataBase("1");
		}else{
			$msg = "";
		}

		$result[0]['data']=unserialize($result[0]['data']);

		$html_data = file_get_contents(JRoute::_("index.php?option=com_mica&view=seishowresults&Itemid=188"));
		$html_encoded = htmlentities($html_data);
		$result[0]['html_data'] = $html_encoded;

		echo json_encode($result);
		die();

		//$app->redirect("index.php?option=com_mica&view=seishowresults&Itemid=188".$msg);

	}

	/**
	 * An ajax task.
	 */
	public function updateWorkspace(){
		$updatedworkspace = workspaceHelper::updateWorkspace();
		$this->saveSldToDataBase("1","1");
		exit;
	}

	/**
	 * An ajax task
	 */
	public function deleteWorkspace(){
		$db      = JFactory::getDBO();
		$session = JFactory::getSession();

		$activeworkspace = $session->get('activeworkspace');
		$query = "DELETE FROM ".$db->quoteName('#__mica_user_custom_attribute')."
			WHERE ".$db->quoteName('profile_id')." = ".$db->quote($activeworkspace);
		$db->setQuery($query);
		$db->execute();

		workspaceHelper::deleteWorkspace();exit;
	}

	/**
	 * An ajax task
	 */
	public function saveWorkspace(){
		$id = workspaceHelper::saveWorkspace(true);
		$this->input->set('workspaceid', $id);
		$this->loadWorkspace("insert");
	}

	/**
	 * An ajax task.
	 */
	public function getMinMax(){
		$session = JFactory::getSession();
		$db      = JFactory::getDBO();

		$value = $this->input->get('value', '', 'raw');

		$activetable = $session->get('activetable');
		$activedata  = $session->get('district');
		$activedata  = explode(",",$activedata);

		$ogrfid = array();
		foreach($activedata as $eachdata){
			$ogrfid[] = $eachdata;
		}

		$result = array();
		if ($value != '') {
			$query = "SELECT (".$value.") as mymin, (".$value.") as mymax
				FROM ".$activetable."
				WHERE ".$db->quoteName('OGR_FID')." IN (".implode(",",$ogrfid).")
				AND (".$db->quoteName($value)." != ".$db->quote("N/A")." AND ".$db->quoteName($value)." != ".$db->quote("NA")." AND ".$db->quoteName($value)." != ".$db->quote("")." AND ".$db->quoteName($value)." != ".$db->quote("N/a") ."  AND ".$db->quoteName($value)." != ".$db->quote("na") .")";
			$db->setQuery($query);
			//echo $query;

			/*$query = "SELECT (".$value.") as mymin, (".$value.") as mymax
				FROM ".$activetable."
				WHERE ".$db->quoteName('OGR_FID')." IN (".implode(",",$ogrfid).")
				AND (".$db->quoteName($value)." != ".$db->quote("N/A")" OR ".$db->quoteName($value)." != ".$db->quote("N/a") " OR ".$db->quoteName($value)." != ".$db->quote("NA")) ;
			$db->setQuery($query);*/
			$result = $db->loadObjectList();
		}

		$min = min($result);
		$max = max($result);
		//echo "<pre>";print_r($min->mymin);exit;
		$results[] = (int)($min->mymin);
		$results[] = (int)($max->mymax)+1;
		//$results[] = ($max->mymax);

		echo implode(",",$results);exit;
	}

	/**
	 * An ajax task.
	 */
	public function getColorGradiant(){
		$value         = $this->input->get('value', '', 'raw');
		$steps         = $this->input->get('steps', '', 'raw');

		$theColorBegin = (isset($value)) ? hexdec("#".$value) : 0x000000;
		$theColorEnd   =  0xffffff;
		$theNumSteps   = (isset($_REQUEST['steps'])) ? intval($steps) : 16;

		$theR0         = ($theColorBegin & 0xff0000) >> 16;
		$theG0         = ($theColorBegin & 0x00ff00) >> 8;
		$theB0         = ($theColorBegin & 0x0000ff) >> 0;

		$theR1         = ($theColorEnd & 0xff0000) >> 16;
		$theG1         = ($theColorEnd & 0x00ff00) >> 8;
		$theB1         = ($theColorEnd & 0x0000ff) >> 0;



		$str = array();
		for ($i = 0; $i <= $theNumSteps; $i++) {
			$theR    = $this->interpolate($theR0, $theR1, $i, $theNumSteps);
			$theG    = $this->interpolate($theG0, $theG1, $i, $theNumSteps);
			$theB    = $this->interpolate($theB0, $theB1, $i, $theNumSteps);
			$theVal  = ((($theR << 8) | $theG) << 8) | $theB;
			$str[$i] =dechex($theVal);
	   }
	   echo implode(",",$str);exit;
	}

	function interpolate($pBegin, $pEnd, $pStep, $pMax) {
		if ($pBegin < $pEnd) {
			return (($pEnd - $pBegin) * ($pStep / $pMax)) + $pBegin;
		} else {
			return (($pBegin - $pEnd) * (1 - ($pStep / $pMax))) + $pEnd;
		}
	}

	/**
	 * An ajax task.
	 */
	public function addthematicQueryToSession(){
		$session = JFactory::getSession();
		$user    = JFactory::getUser();
		if($user->id == 0){
			return -1;
		}

		$formula = $this->input->get('formula', '', 'raw');
		$level   = $this->input->get('level', '', 'raw');
		if($level == ""){
			$level = $this->getSldLevel();
			$level = explode(",",$level);
			$level = $level[0];
		}

		$from  = $this->input->get('from', '', 'raw');
		$to    = $this->input->get('to', '', 'raw');
		$color = $this->input->get('color', '', 'raw');

		$customformulafromsession = $session->get('customformula');

		$geteachformula = explode("|",$customformulafromsession);
		foreach($geteachformula as $key => $eachtocheck){
			$singleformula = explode(":",$eachtocheck);

			if($singleformula[0] == $formula ||$singleformula[0] == ""){
				unset($geteachformula[$key]);
			}
		}
		$customformulafromsession = implode("|",$geteachformula);
		$customformula = "|".$formula.":".$from."->".$to."->".$color."->".$level;
		$customformulafromsession = $customformulafromsession.$customformula;
		$customformulafromsession = explode(":",$customformulafromsession);
		$customformulafromsession = array_filter($customformulafromsession);
		$customformulafromsession = array_unique($customformulafromsession);
		$customformulafromsession = implode(":",$customformulafromsession);
		$session->set('customformula',$customformulafromsession);
		//Edited
		$session->set('fromthematic',1);
		//Edited ends

		echo $customformulafromsession;exit;
	}

	/**
	 * An ajax task., internal as well.
	 */
	public function deletethematicQueryToSession($innercall = null){
		$session = JFactory::getSession();
		$user    = JFactory::getUser();
		$db      = JFactory::getDBO();
		if($user->id == 0){
			return -1;
		}

		$formula = $this->input->get('formula', '', 'raw');
		$level   = $this->input->get('level', '', 'raw');
		//$from  =$this->input->get('from', '', 'raw');
		//$to    =$this->input->get('to', '', 'raw');
		//$color =$this->input->get('color', '', 'raw');

		$customformulafromsession = $session->get('customformula',$attributes);
		$geteachformula = explode("|",$customformulafromsession);

		foreach($geteachformula as $key => $eachtocheck){
			$singleformula = explode(":",$eachtocheck);
			if($singleformula[0] == $formula){
				unset($geteachformula[$key]);
			}
		}
		$customformulafromsession   = implode("|",$geteachformula);
		//$customformula            = $formula.":".$from."->".$to."->".$color."|";
		//$customformulafromsession = $customformulafromsession.$customformula;
		$customformulafromsession   = explode(":",$customformulafromsession);
		$customformulafromsession   = array_filter($customformulafromsession);
		$customformulafromsession   = array_unique($customformulafromsession);
		$customformulafromsession   = implode(":",$customformulafromsession);
		//echo $customformulafromsession;
		$session->set('customformula',$customformulafromsession);

		$workspaceid = $session->get('activeworkspace');
		$query = "DELETE FROM ".$db->quoteName('#__mica_sld_legend')."
			WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($workspaceid)."
				AND ".$db->quoteName('level')." = ".$db->quote($level);
		$db->setQuery($query);
		$db->execute();

		$query="DELETE FROM ".$db->quoteName('#__mica_map_sld')."
			WHERE ".$db->quoteName('profile_id')." = ".$db->quote($workspaceid)."
				AND ".$db->quoteName('level')." = ".$db->quote($level);
		$db->setQuery($query);
		$db->execute();

		if($innercall != null){
			return true;
		}else{
			exit;
		}
	}


	/**
	 * An internal task.
	 */
	private function saveSldToDataBase($innercall = null, $isUpdate = null){
		cssHelper::saveSldToDatabase($innercall, $isUpdate);
	}

	function getSldLevel(){
		$db      = JFactory::getDBO();
		$session = JFactory::getSession();

		$activeworkspace=$session->get('activeworkspace');

		$query="SELECT level FROM #__mica_sld_legend where workspace_id = ".$activeworkspace." group by level";
		$db->setQuery($query);
		$result =$db->loadObjectList();

		$str   ="";
		$level ="0,1,2";
		foreach($result as $eachlevel){
			$level =str_replace($eachlevel->level.",","",$level);
		}
		return $level;
	}

	/**
	 * Ajax request to export chat in PDF.
	 */
	public function amExport(){

		$imgtype = $this->input->get('imgtype', '', 'raw');
		//echo "<pre/>";print_r($imgtype);exit;
		$name    = $this->input->get('name', '', 'raw');

		if(!$imgtype){ // To be sure that the parameters are correctly forwarded
			echo "no image type";exit;
		}

		$imgquality = 100;// set image quality (from 0 to 100, not applicable to gif)
		$data       = &$_POST;// get data from $_POST or $_GET ?

		// get image dimensions
		$width  = (int) $data['width'];
		$height = (int) $data['height'];
		$img    = imagecreatetruecolor($width, $height);// create image object

		// populate image with pixels
		for ($y = 0; $y < $height; $y++) {
			$x   = 0; // innitialize
			$row = explode(',', $data['r'.$y]);// get row data
			$cnt = sizeof($row);// place row pixels

			for ($r = 0; $r < $cnt; $r++) {
				$pixel    = explode(':', $row[$r]);// get pixel(s) data
				$pixel[0] = str_pad($pixel[0], 6, '0', STR_PAD_LEFT);// get color

				$cr_e     = '0x'.strtoupper(substr($pixel[0], 0, 2));
				$cg_e     = '0x'.strtoupper(substr($pixel[0], 2, 2));
				$cb_e     = '0x'.strtoupper(substr($pixel[0], 4, 2));

				/*
					// VALORI DECIMALI
					$cr = hexdec(substr($pixel[0], 0, 2));
					$cg = hexdec(substr($pixel[0], 2, 2));
					$cb = hexdec(substr($pixel[0], 4, 2));
				*/

				$color = imagecolorallocatealpha($img, hexdec($cr_e), hexdec($cg_e), hexdec($cb_e), '0');// allocate color
				$repeat = isset($pixel[1]) ? (int) $pixel[1] : 1;// place repeating pixels
				for ($c = 0; $c < $repeat; $c++) {
					imagesetpixel($img, $x, $y, $color);// place pixel
					$x++;// iterate column
				}
	  		}
		}

		$path = JPATH_BASE."/userchart/001.".$imgtype; // relative or absolute path to server; check permissions to write on that dir!
		switch($imgtype){
			case 'png': imagepng($img,$path) or die("Unable to create the file");
				break;
			case 'jpeg': imagejpeg($img,$path) or die("Unable to create the file");
				break;
			case 'jpg': imagejpeg($img,$path) or die("Unable to create the file");
				break;
			case 'gif': imagegif($img,$path) or die("Unable to create the file");

				break;
		}

		$logo = JPATH_BASE."/templates/mica/images/logo_pdf_chart.png";
		$pdfhelper = new pdfHelper();
		$pdfhelper->newPage();
		$pdfhelper->addHeader("",$logo);
		$pdfhelper->addImage($path);
		$pdfhelper->setOutput($name.".pdf");
		exit;

		//	    // set proper content type
		//	    $ch=fopen($path);
		//	    $content=fread($ch,filesize($path));
		//	   $ch1=fopen('/var/www/micamap/'.$name.".pdf",'w');
		//	   //write image to file
		//	   fwrite($ch1,@readfile($path),filesize($path));
		//	   fclose($ch1);
		//	   imagedestroy($img);
		//	    header('Content-type: application/pdf');
		//	    header('Content-Disposition: attachment; filename="'.$name.'.pdf"');
		//	    header("Content-Transfer-Encoding: binary");
		//	    header('Content-Length: ' . filesize($path));
		//		@readfile('/var/www/micamap/'.$name.".pdf");
		//
		//
		//	exit;
	}

	/**
	 * A task for excel export.
	 */
	public function exportexcel(){
		$session = JFactory::getSession();
		$model   = $this->getModel();//'seishowresults','seishowresultsModel'
		$model->getCustomData(1,1);
		//$model->getDisplayGroup();
		$groupwisedata = $model->getDisplayGroup();
		//print_r($groupwisedata);exit;
		//$groupwisedata=$session->get('Groupwisedata');

		$Market  = array();
		$Market1 = array();

		foreach($groupwisedata as $grp=>$grpdatas){
			foreach($grpdatas as $grpname=>$grpdata){
				if($grpname == "Mkt Potential Index"){
					$tmp[$grpname]=$grpdata;
				}else if($grpname=="Score" || $grpname=="Composite Score" ){
					$tmp1[$grpname]=$grpdata;
				}else{
					$rearrangeddata[$grpname]=$grpdata;
				}
			}

			$Market  = $tmp;
			$Market1 = $tmp1;

			if(!is_array($tmp1)){
				$groupwisedatas[$grp]=array_merge($Market,$rearrangeddata);
			}else{
				$groupwisedatas[$grp]=array_merge($Market,$Market1,$rearrangeddata);
			}
		}


		//array_pop($groupwisedatas);

		$i = 0;
		$j = 0;
		$k = 0;
		$l = 0;
		$headerspan = 0;

		foreach($groupwisedatas[1] as $mainname => $data){
			$header[$i] = $mainname;
			foreach($data as $grpname => $grpdata){
				$grpheader[$i][$j] = JTEXT::_($grpname).str_repeat("\t",($model->getColspan($grpdata,2)));
				$headerspan += $model->getColspan($grpdata,2);
				foreach($grpdata as $variablegrp => $variabledata){
					$vargrpname[$i][$k] = $variablegrp.str_repeat("\t",($model->getColspan($variabledata,3)));
					foreach($variabledata as $eachvariabledata){
						unset($eachvariabledata['key']);
						foreach($eachvariabledata as $key => $value){
							$mygrpvariablename[$i][$k][$l] = JTEXT::_($key)."\t";
							$mygrpdata[$i][$k][$l]         = $value."\t";
						}
						$l++;
					}
					$k++;
				}
				$j++;
			}
			$i++;
		}

		/*echo "<pre/>";print_r($mygrpdata);exit;
		echo "<pre>";print_R($grpheader);print_R($vargrpname);print_R($mygrpvariablename);print_r($mygrpdata);exit;*/
		//$op=$session->get('state');
		$op = "State";
		if($session->get('district')!=""){
			$m_type=$session->get('m_type');
			if($m_type=="Total"){
				$m_type="All";
			}
			$op .="-> District ";
		}else if($session->get('urban')!=""){
			$op .="->Urban Agglomeration ";
		}else if($session->get('town')!=""){
			$op .="->Town";
		}

		$str =  str_repeat("\t",($model->getColspan($headerspan,2)/2))."Data of ".$op.str_repeat("\t",($model->getColspan($headerspan,2)/2))."\n";
		$str .= "\t";
		foreach($grpheader as $inserheader){
			foreach($inserheader as $inserheaderval){
				$str .=$inserheaderval;
			}
			break;
		}
		$str .="\n";
		$str .="\t";
		foreach($vargrpname as $eachgrpname)
		{
			foreach($eachgrpname as $name)
			{
				$str .=$name;
			}
			break;
		}
		$str .="\n";

		$str .="\t";
		foreach($mygrpvariablename as $variablenamegrp){
			foreach($variablenamegrp as $varname){
				foreach($varname as $each){
					$str .=$each;
				}
			}
			break;
		}

		$str .="\n";
		foreach($mygrpdata as $key=>$gdata){
			$str .=$header[$key]."\t";
			foreach($gdata as $data){
				foreach($data as $d){
					$str .=$d;
				}
			}
			$str .="\n";
		}

		header('Content-type: application/ms-excel');
		header('Content-Disposition: attachment; filename=datasheet.xls');
		echo $str;exit;
	}

	function exportMap(){
		$session =JFactory::getSession();
		$db      =JFactory::getDBO();

		$activeworkspace=$session->get('activeworkspace');

		$query =" SELECT * FROM ".$db->quoteName('#__mica_sld_legend')."
			WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($activeworkspace)."
			ORDER BY ".$db->quoteName('level').",".$db->quoteName('range_from')." ASC";
		$db->setQuery($query);
		$result = $db->loadObjectList();

		$id    =array();
		$level =array();
		$i     =1;
		$j     =1;
		$html  ="";

		foreach($result as $range){
			if($range->level == "0"){
				$str ='<td bgcolor="'.$range->color.'" width="15" > </td>';
			}else{
				$pin=str_replace("#","",$range->color);
				$pinimage=JPATH_BASE.'/components/com_mica/maps/img/layer'.trim($range->level).'/pin'.$pin.'.png';
				//$pinimage='../maps/img/layer'.trim($range->level).'/pin'.$pin.'.png';
				$str ='<td ><img src="'.$pinimage.'"  alt="pin" border="1" width="20"/></td>';
			}
			$grouping[$range->custom_formula][]='<tr><td  width="100" >'.$range->range_from.' - '.$range->range_to."</td>".$str."</tr>";
			//$id[]=$range->custom_formula;
			//$grouping[$range->custom_formula]=$range->level;
			$level[$range->custom_formula][]=$range->level;
			$i++;
			$j++;
		}

		$i            =0;
		$range        =array();
		$str          =array();
		$totalcount   =0;
		$l            =0;
		$tojavascript =array();
		foreach($grouping as $key=>$val){
			$grname         =$this->getCustomAttrName($key,$activeworkspace);
			$grpname[]      =$grname;
			$str[]          =implode(" ",$val);
			$ranges[]       =count($val);
			$levelunique[]  =$level[$key][$l];
			$tojavascript[] =$key;
			$l++;
		}
		$tojavascript =implode(",",$tojavascript);
		//$str        ="";
		$op           =$session->get('state');
		if($session->get('district')!=""){
			$m_type=$session->get('m_type');
			// if(is_array($m_type))
			// {
			// 	$m_type = implode(",",$m_type);
			// }
			if($m_type=="Total")
			{
				$m_type="All";
			}
			$op .="-> District ";
		//	$op .="-> District (".$m_type.")";
		}else if($session->get('urban')!=""){
			$op .="->Urban Agglomeration ";
		}else if($session->get('town')!=""){
			$op .="->Town";
		}

		$html='';

		if($grpname!=""){
			for($i=0;$i<count($grpname);$i++){
				$html .= '<td><table  cellspacing="5" cellpadding="0"   border="0"><tr><td class="range"  colspan="2"><b>'.$grpname[$i][0].'</b></td></tr>'.$str[$i].'</table></td>';
			}
		}else{
			//$html .='<tr><td >{mapimage}</td></tr>';
		}




		$mapparameter  =rawurlencode($this->input->get('mapparameter', '', 'raw'));
		$mapparameter1 =rawurlencode($this->input->get('baselayer', '', 'raw'))."&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&SRS=EPSG%3A4326&FORMAT=image%2Fpng&LAYERS=india%3Arail_state&TRANSPARENT=true&styles=dummy_state&WIDTH=925&HEIGHT=650";
		$makeurl       ="";
		$mapparameter  =str_replace("EQT","=",$mapparameter);
		$mapparameter  =str_replace("AND","&",$mapparameter);
		$mapparameter  =str_replace('\n'," ",$mapparameter);
		$mapparameter  =$this->tomcat_url."?".$mapparameter;

		$mapparameter1 =str_replace("EQT","=",$mapparameter1);
		$mapparameter1 =str_replace("AND","&",$mapparameter1);

		$mapparameter1 =str_replace('\n'," ",$mapparameter1);
		$mapparameter1 =$this->tomcat_url."?".$mapparameter1;
		//$image= getimagesize($mapparameter);
		$layer1    =JPATH_BASE."/userchart/map.png";
		$baselayer =JPATH_BASE."/userchart/base.png";
		$finalop   =JPATH_BASE."/userchart/final.png";

		file_put_contents($layer1, file_get_contents($mapparameter));
		file_put_contents($baselayer, file_get_contents($mapparameter1));

		$dest = imagecreatefrompng($baselayer);
		$src  =  imagecreatefrompng($layer1);
		imagecopy($dest,$src,0,0,0,0,imagesx($src),imagesy($src));
		imagepng($dest,$finalop);
		imagedestroy($dest);
		//file_put_contents($finalop, $final_img);
		require_once(JPATH_BASE.'/components/com_mica/helpers/tcpdf/tcpdf.php');
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		ob_clean();

		// set default header data
		$logo="logo_pdf.png";
		$pdf->SetHeaderData($logo, '30', '');
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		//$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->SetFont('times', '', 8);
		$y = $pdf->getY();
		$pdf->SetFillColor(255, 255, 0);
		// set color for text
		$pdf->SetTextColor(0, 0, 0);

		$pdf->AddPage();
		$rowspan=(count($result)+count($grpname))+2;

		$mapimage='<img src="'.JPATH_BASE.'/userchart/final.png"  border="0"/>';
		$head = "";
$html = <<<EOD
<table><tr><td ><b>Data of $op</b></td></tr>
<tr><td >$mapimage</td></tr>
<tr><td >
<table width="500"><tr>$html</tr></table>
</td></tr>
</table>
EOD;


		// Place image relative to end of HTML
		$pdf->writeHTML($html, true, false, false, false, '');
		//$pdf->writeHTMLCell(PDF_MARGIN_LEFT, $y, PDF_MARGIN_LEFT, $y, $tbl, '', 1, 0, true, 'R', true);
		//$x =$pdf->GetX();
		//$y =$pdf->GetY();
		//$pdf->SetXY($x, $y);
		//$pdf->Image(JUri ::base().'userchart/final.png');
		//$pdf->writeHTML($html,true, true, true, true, '');
		$pdf->lastPage();
		$pdf->Output('map.pdf', 'D');
		exit;

		/* $pdfhelper = new pdfHelper();
		$pdfhelper->newPage();
		$pdfhelper->addHeader("MIMI",$logo);
		$pdfhelper->addImage($finalop,60,30,650,650,'PNG');
		$pdfhelper->addHtml(5,$html);

		$pdfhelper->setOutput("map.pdf");*/
	}

	function getCustomAttrName($name,$activeworkspace){
		$db = JFactory::getDBO();

		$query = "SELECT name, attribute
			FROM ".$db->quoteName('#__mica_user_custom_attribute')."
			WHERE ".$db->quoteName('profile_id')." = ".$db->quote($activeworkspace)."
				AND ".$db->quoteName('attribute')." LIKE '".$name."'";
		$db->setQuery($query);
		$result = $db->loadAssoc();

		if(count($result) == 0){
			return array($name, $name);
		}else{
			return array($result['name'], $result['attribute']);
		}
	}

	/**
	 * A task page.
	 */
	public function downloadMatrix(){
		$session = JFactory::getSession();
		$model   = $this->getModel();

		$MaxForMatrix = $model->getMaxForMatrix();

		$data         = $model->getCustomData(1,1);
		$customdata   = $data;

		$keys    =array_keys($MaxForMatrix[0]);
		$vals    =array_values($MaxForMatrix[0]);
		//$width =1024/count($keys)+1;
		//$width =round($width);
		$width   = 150;

		$table='<table cellpadding="0" cellspacing="0" border="0">';
		$table .= '<tr bgcolor="#0154A1" ><th width="'.$width.'">Legends</th><th width="'.$width.'">Variable Name</th>';
		foreach($keys as $eachkey){
			$table .= '<th width="'.$width.'">'.JTEXT::_($eachkey).'</th>';
		}
		$table .= '</tr>';

		$i = 0;
		//$classarray = array("#f39685","#b8413d","#a61b18","#7f0301","");
			/*$classname    = array("#ffe6aa"=>"Low","#8ee99d"=>"Medium","#ffb1c1"=>"High","#ff4949"=>"Very High");
		     $classarray = array("#ECE0BA","#CC9966","#89570F","#5C3B0F","");*/

		$classarray   = array("#ffe6aa","#8ee99d","#ffb1c1","#ff4949");
		$classname    = array("#ffe6aa" => "Low","#8ee99d" => "Medium","#ffb1c1" => "High","#ff4949" => "Very High");
		foreach($customdata as $eachdata){
			$table .= '<tr>';
			if(isset($classarray[$i]) && $classarray[$i]!=""){
				$table .= '
					<td width="'.$width.'">
						<table cellspacing="6" cellpadding="4">
							<tr>
								<td bgcolor="'.$classarray[$i].'">'.$classname[$classarray[$i]].'</td>
							</tr>
						</table>
					</td>';
			}else{
				$table .= '<td width="'.$width.'"></td>';
			}

			$i++;

			$table .= '<td width="'.$width.'" >'.JText::_($eachdata->name).'</td>';
			foreach($eachdata as $key=>$val){
				$q  =$MaxForMatrix[0][$key]/4;
				$q1 =$q+$q;
				$q2 =$q+$q+$q;
				$q4 =$MaxForMatrix[0][$key];

				if(in_array($key,$keys) && $key!="name"){

					if($val<=$q){
						$class="#ffe6aa";
					}else if($val>=$q && $val<$q1){
						$class="#8ee99d";
					}else if($val>=$q1 && $val<$q2){
						$class="#ffb1c1";
					}else if($val>=$q2 && $val<=$q4){
						$class="#ff4949";
					}

					if($val=='N/a'){
						//$class="#000000";
						$table .= '<td bgcolor="'.$class.'" width="'.$width.'" style="border:1px solid #FFFFFF;">'.$val.'</td>';//
					}else{
						$table .= '<td bgcolor="'.$class.'" width="'.$width.'" style="border:1px solid #FFFFFF;"></td>';//
					}
					//$table .= '<td bgcolor="'.$class.'" width="'.$width.'" style="border:1px solid #FFFFFF;"></td>';//
				}
				//echo "<pre>"; print_r($this->customdata);print_r($keys);print_r($vals);echo "</pre>";
			}
			$table .= "</tr>";
		}

		if(count($eachdata)<=3 && $i<=3){
			for($i=3;$i<count($classarray);$i++){
				$table .= '<tr>
					<td width="'.$width.'">
						<table>
							<tr>
								<td bgcolor="'.$classarray[$i].'">'.$classname[$classarray[$i]].'</td>
							</tr>
						</table>
					</td>';

				$td    = '<td width="'.$width.'"></td>';
				$table .= str_repeat($td,count($this->customdata[0]));
				$table .= '</tr>';
			}
		}
		$table .= '</table>';
		header('Content-type: application/ms-excel');
		header('Content-Disposition: attachment; filename=datasheet.xls');
		echo $table;exit;
	}

	/**
	 * a task for page.
	 */
	public function setFieldVal(){
		$session = JFactory::getSession();

		$activetable = $session->get("activetable");
		$id          = $this->input->get('id', '', 'raw');

		if($activetable  == "india_information"){
			$dist = $session->get("district");
			$session->set('district', str_replace($id, "", $dist));
			$dist = $session->get("district");
		}

		if($activetable  == "rail_state"){
			$state =$session->get("state");
			$session->set('state', str_replace($id, "", $state));
			$dist  =$session->get("state");
		}

		if($activetable  == "jos_mica_urban_agglomeration"){
			$urban = $session->get("urban");
			$session->set('urban', str_replace($id, "", $urban));
			$dist  = $session->get("urban");
		}

		if($activetable  == "my_table"){
			$town = $session->get("town");
			$session->set('town', str_replace($id, "", $town));
			$dist = $session->get("town");
		}

		$app = JFactory::getApplication();
		$app->redirect("index.php?option=com_mica&view=seishowresults&Itemid=188");
	}

	/**
	 * An ajax task.
	 */
	public function getMaxForMatrix($innercall = null){
		$session = JFactory::getSession();
		$db      = JFactory::getDBO();

		$table         = $session->get('activetable');
		$variable      = $session->get('themeticattribute');
		$district      = $session->get('district');

		$variable      = explode(",",$variable);
		$maxValsSelect = "";
		foreach($variable as $eachvariable){
			$maxValsSelect[] = "MAX(Convert(" . $eachvariable . ",SIGNED)) AS '" . $eachvariable . "'";
			/*SELECT MAX(Convert(`Total_MPI`,SIGNED)) AS 'Total_MPI' , MAX(Convert(`Total_Female_Population`,SIGNED)) AS 'Total_Female_Population' FROM india_information*/
		}
		$maxValsSelect = implode(" , ",$maxValsSelect);
		//die($maxValsSelect);

		//Complete the query
		$maxValsQuery = "SELECT $maxValsSelect FROM $table WHERE OGR_FID IN (".$district.") LIMIT 0, 1";

		$db->setQuery($maxValsQuery);
		if($innercall == 1){
			return $db->loadAssocList();
		}

		$MaxForMatrix = $db->loadAssocList();
		$model        = $this->getModel();
		$data         = $model->getCustomData(1,1);
		$customdata   = $data;


		echo '<div class="topheader row"><div class="col-md-8">';

		echo '<label>Legends</label>';

		echo "<ul>";
		$i = 0;
		//$classarray = array("#f39685","#b8413d","#a61b18","#7f0301","");
		//$classarray   = array("#ffe6aa","#8ee99d","#ffb1c1","#ff4949","");
		//$classname  = array("#f39685"=>"Low","#b8413d"=>"Average","#a61b18"=>"Medium","#7f0301"=>"High",""=>"");
		$classname    = array("#ffe6aa"=>"Low","#8ee99d"=>"Medium","#ffb1c1"=>"High","#ff4949"=>"Very High");
		$classarray = array("#ECE0BA","#CC9966","#89570F","#5C3B0F","");
		//$classname  = array("#ECE0BA"=>"Low","#CC9966"=>"Average","#89570F"=>"Medium","#5C3B0F"=>"High",""=>"");
		foreach($classname as $key =>$value){


				echo "<li width='".$width."px' >".$value."<div  style='float:right;width:40px;height:14px;margin:2px 0px 2px 15px; border:1px solid #ccc; background-color:".$key.";'></li>";

			$i++;
		}
		echo '</ul>';
		echo "</div><div class='col-md-4'>
					<div class='button-style'><input style='margin-left: 3px;' type='button' class='frontbutton ' name='fullscreen' value='Download' id='downmatrix'/></td><td class='frontbutton' style='min-width: 75px !important;'><input style='margin-right: 3px;' type='button' class='frontbutton fullscreenicon' name='fullscreen' value='Full Screen'/></div>
			  </div></div>";

		echo '<div class="scroller_quartiles_table"> <table   cellpadding="0" cellspacing="0" >';

		$keys  = array_keys($MaxForMatrix[0]);
		$vals  = array_values($MaxForMatrix[0]);
		$width = 1024/count($keys)+1;

		//kamlesh check here
		echo "<tr id='matrixhead'>";
		//echo "<td width='".$width."px'>Legends</td>";
		echo "<td width='".$width."px'>Variable Name</td>";
		foreach($keys as $eachkey){
			//echo "<td width='".$width."px'><a href='javascript:void();' title='".JTEXT::_($eachkey)."' style='color:white !important;'>".ucwords(strtolower(str_replace("_"," ",substr($eachkey,0,10))))."</td>";
			echo "<td width='".$width."px'><a href='javascript:void();' title='".JTEXT::_($eachkey)."' style='color:white !important;'>".substr(JTEXT::_($eachkey),0,10)."</td>";
		}
		echo "</tr>";
		$i = 0;

		//$classarray = array("#f39685","#b8413d","#a61b18","#7f0301","");
		$classarray   = array("#ffe6aa","#8ee99d","#ffb1c1","#ff4949","");
		//$classname  = array("#f39685"=>"Low","#b8413d"=>"Average","#a61b18"=>"Medium","#7f0301"=>"High",""=>"");
		$classname    = array("#ffe6aa"=>"Low","#8ee99d"=>"Medium","#ffb1c1"=>"High","#ff4949"=>"Very High",""=>"");
		//$classarray = array("#ECE0BA","#CC9966","#89570F","#5C3B0F","");
		//$classname  = array("#ECE0BA"=>"Low","#CC9966"=>"Average","#89570F"=>"Medium","#5C3B0F"=>"High",""=>"");

		foreach($customdata as $eachdata){
			//echo $eachdata->OGR_FID."DDD";exit;
			//if($eachdata->OGR_FID == 363)
				//continue;

			echo "<tr>";
			// if(isset($classarray[$i]) && $classarray[$i]!=""){
			// 	//echo "<td width='".$width."px' >".$classname[$classarray[$i]]."<div  style='float:right;width:10px;height:10px;margin-right: 2px;background-color:".$classarray[$i].";'></td>";
			// }else{
			// 	if($i==5){
			// 		echo "<td width='".$width."px' class='frontbutton'></td>";
			// 	}else if($i==4){
			// 		echo "<td width='".$width."px' class=''><table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'></td></tr></table></td>";
			// 	}else{
			// 		echo "<td width='".$width."px'></td>";
			// 	}
			// }
			//$i++;

			echo "<td width='".$width."px' >".JText::_($eachdata->name)."</td>";
			 
			/*if($_SERVER['REMOTE_ADDR'] == '122.170.14.145'){
				//echo $maxValsQuery;
				print "<pre>";
				print_r($district);
				/*print "<pre>";
				print_r($MaxForMatrix);*/

				/*foreach($eachdata as $key=>$val){
				echo '<br/>Q:<br/>'.$q  = $MaxForMatrix[0][$key]/4;
				echo '<br/>Q1:<br/>'.$q1 = $q+$q;
				echo '<br/>Q2<br/>'.$q2 = $q+$q+$q;
				echo '<br/>Q4<br/>'.$q4 = $MaxForMatrix[0][$key];
				}*/
			/*}*/
			
			
	
			foreach($eachdata as $key=>$val){
				$q  = $MaxForMatrix[0][$key]/4;
				$q1 = $q+$q;
				$q2 = $q+$q+$q;
				$q4 = $MaxForMatrix[0][$key];


				if(in_array($key,$keys) && $key!="name")
				{

					if($val<=$q){
						$class="#ffe6aa";//$class="#f39685";
					}else if($val>=$q && $val<$q1){
						$class="#8ee99d";
					}else if($val>=$q1 && $val<$q2){
						$class="#ffb1c1";
					}else if($val>=$q2 && $val<=$q4){
						$class="#ff4949";
					}

					if($val=='N/a'){
						//$class="#000000";
						echo '<td  width="'.$width.'px" style="background-color:'.$class.';border:1px solid #FFFFFF;text-align:center;" >'.$val.'</td>';//
					}else{
						echo '<td  width="'.$width.'px" style="background-color:'.$class.';border:1px solid #FFFFFF;" ></td>';//
					}
				}
				//echo "<pre>"; print_r($customdata);print_r($keys);print_r($vals);echo "</pre>";
			}
			echo "</tr>";
		}

		if(count($eachdata)<=3 && $i<=3){
			for($i=count($eachdata);$i<=count($classarray);$i++){
				echo "<tr>";
				// if($i==5){
				// 	echo "<td width='".$width."px' class='frontbutton'></td>";
				// }else if($i==4){
				// 	echo "<td width='".$width."px' class=''><table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'></td></tr></table></td>";
				// }else{
				// 	echo "<td width='".$width."px' >".$classname[$classarray[$i]]."<div class=''style='float:right;width:10px;height:10px;margin-right: 2px;background-color:".$classarray[$i].";'></td>";
				// }

				$td = "<td width='".$width."px'></td>";
				echo str_repeat($td,count($customdata[0]));
				echo "</tr>";
			}
		}

		echo '</td></tr></table></div>';exit;
	}

	/**
	 * An ajax task.
	 */
	public function speedometer(){
		$model = $this->getModel();//'seishowresults','seishowresultsModel'
		$data  = $model->getCustomData(1,1);
		$str   = "";
		foreach($data as $eachdata){
			$str .= '<option value='.str_replace(" ","%20",$eachdata->name).'>'.$eachdata->name.'</option>';
		}
		echo $str;exit;
	}

	/**
	 * An ajax task.
	 */
	public function getGraph(){
		$db = JFactory::getDBO();

		$dist = $this->input->get('dist', '', 'raw');
		$attr = $this->input->get('attr', '', 'raw');

		$query = " SELECT ".$attr."  FROM ".$db->quoteName('india_information')." WHERE ".$db->quoteName('OGR_FID')." IN (".$dist.")";
		$db->setQuery($query);
		try {
			$attr = $db->loadObjectList();
		} catch (Exception $e) {
			$attr = NULL;
		}

		foreach($attr as $eachattr){
			foreach($eachattr as $key=>$val){
				$attrs[JTEXT::_($key)][]=$val;
			}
		}

		//echo "<pre>";print_r($attrs);exit;
		//$attr=array_merge($attr,$eachcustomattr);
		$arraytoreturn = "";
		$lastkey       = count($attr);
		$z             = 1;
		foreach($attrs as  $key => $val){
			if($z!==$lastkey){
				$arraytoreturn .=JTEXT::_($key).";".implode(";",$val)."sln";
			}else{
				$arraytoreturn .=JTEXT::_($key).";".implode(";",$val);
			}
			$z++;
		}

		$i=0;
		$query = "SELECT concat(distshp,'-',state) as name, OGR_FID FROM ".$db->quoteName('india_information')." WHERE ".$db->quoteName('OGR_FID')." IN (".$dist.")";
		$db->setQuery($query);
		$dist = $db->loadObjectList();

		foreach($dist as $eachdata){
			$data_settings .= "<graph gid='".$i."'>";
			$data_settings .= "<title><![CDATA[".JTEXT::_($eachdata->name)."]]></title>";
			$data_settings .= "</graph>";
			$i++;
			if($i==15){
				break;
			}
		}
		echo $arraytoreturn."<->".$data_settings;exit;
	}

	public function getMeterajax()
	{
		$model = $this->getModel();
		$data  = $model->getMeterData();

		echo json_encode($data);
		die();
	}

	/*
		Get Selected Variable List for Custom Varible
	*/
	public function getSelectedVarListForCustom()
	{
		$model = $this->getModel();
		$data  = $model->getSelectedVarListForCustom();

		echo $data;
		die();
	}

	/*
		Get Selected Variable List for Custom Varible
	*/
	public function getSelectedVarListForSpeedoMeter()
	{
		$model = $this->getModel();
		$data  = $model->getSelectedVarListForSpeedoMeter();

		echo $data;
		die();
	}

	/*
		Get Selected District List for Graph
	*/
	public function getSelectedDistListForGraph()
	{
		$model = $this->getModel();
		$data  = $model->getSelectedDistListForGraph();

		echo $data;
		die();
	}

	/*
		Get Selected Variable List for Graph
	*/
	public function getSelectedVarListForGraph()
	{
		$model = $this->getModel();
		$data  = $model->getSelectedVarListForGraph();

		echo $data;
		die();
	}

	/**
	 * A task to provide district datas as options for select list.( AJAX call)
	 */
	public function getDistrictlist(){
		$dist = $this->input->get('dist', '', 'raw');

		$dist_explode = explode(",", $dist);
		$list         = array();
		if (count($dist_explode) > 0  && $dist_explode[0] > 0) {
			$db = JFactory::getDBO();
			$query="SELECT CONCAT(distshp,'-',state) as name , OGR_FID
				FROM ".$db->quoteName('india_information')."
				WHERE ".$db->quoteName('OGR_FID')." IN (".$dist.")
				GROUP BY ".$db->quoteName('distshp')."
				ORDER BY ".$db->quoteName('distshp')." ASC";
			$db->setQuery($query);
			$list = $db->loadObjectList();
		}

		$options = "";
		foreach($list as $eachlist){
			$options .="<li width='100px;float:left;'> <input type='checkbox' class='districtchecked' value='".$eachlist->OGR_FID."' />".$eachlist->name."</li>";
		}

		echo $options;exit;
	}

	function forceFileDownload() {
		$user = JFactory::getUser();
		if($user->id != 0) {
			header("Content-Type: application/force-download");
			header("Content-Transfer-Encoding: Binary");
			$path = JURI::root().'components'.DIRECTORY_SEPARATOR.'com_mica'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'MIMI Urban Agglomeration.xls';
			header("Content-disposition: attachment; filename=\"MIMI Urban Agglomeration.xls\"");
			header("Location: $path");
		}
	}

	/**
	 * Let you download spreadsheet of Town data file.
	 * @param string $value [description]
	 */
	public function TownforceFileDownload(){
		$user = JFactory::getUser();
		if($user->id != 0) {
			header("Content-Type: application/force-download");
			header("Content-Transfer-Encoding: Binary");
			$path = JURI::root().'components'.DIRECTORY_SEPARATOR.'com_mica'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'MIMI Towns.xlsx';
			header("Content-disposition: attachment; filename=\"MIMI Towns.xlsx\"");
			header("Location: $path");
		}
	}

	/**
	 * Let you download spreadsheet of Town Definition data file.
	 * @param string $value [description]
	 */
	public function TownsDefinitionforceFileDownload(){
		$user = JFactory::getUser();
		if($user->id != 0) {
			header("Content-Type: application/force-download");
			header("Content-Transfer-Encoding: Binary");
			$path = JURI::root().'components'.DIRECTORY_SEPARATOR.'com_mica'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'MiMi-TownsDefinition.xlsx';
			header("Content-disposition: attachment; filename=\"MiMi-TownsDefinition.xlsx\"");
			header("Location: $path");
		}
	}

	/*
		Get Graph Data
	 */
	public function getGraphajax()
	{
		$model = $this->getModel();
		$data = $model->getGraph_v2();
		echo json_encode($data);
		die();
	}

	public function getDataajax()
	{
		$model = $this->getModel('seishowresults');
		$app = JFactory::getApplication();
		$AttributeTable  = $model->getNewAttributeTable();// $this->get('AttributeTable');

		echo $AttributeTable;exit;

		// $graph           = $this->get('Graph');
		// $GraphSettings   = $this->get('GraphSettings');

		// $activesearchvariable       = $session->get('activesearchvariable');
		// $this->activesearchvariable = $activesearchvariable;
		// $this->zoom                 = $zoom;
		/*$tomcaturl                  = TOMCAT_URL;
		$this->tomcaturl            = $tomcaturl;*/

		// $BreadCrumbcontent = $this->get('BreadCrumb');
		// list($BreadCrumb, $workspacelink) = explode("~~~", $BreadCrumbcontent);
		// $this->workspacelinkdiv   = $workspacelink;
		// $this->BreadCrumb         = $BreadCrumb;

		// $geometry                 = $this->get('InitialGeometry');
		// $UnselectedDistrict       = $this->get("UnselectedDistrict");
		// $this->UnselectedDistrict = $UnselectedDistrict;
		// $this->geometry           = $geometry;


		// $this->user               = JFactory::getUser();
		// $this->userid             = $this->user->id;
		// $this->activeworkspace    = $session->get('activeworkspace');

		// $this->is_default         = $session->get('is_default');
		// $this->customattribute    = $customattribute;

		// $this->get('customattrlib');
		// $this->customattributelib = $session->get('customattributelib');
		// $this->AttributeTable     = $AttributeTable;
		// $this->GraphSettings      = $GraphSettings;
		// $this->graph              = $graph;

		// /*$this->m_type = $m_type;$app->input->set('m_type',$m_type);*/
		// $this->attributes        = $addtosession;
		// $this->new_name          = $new_name;
		// $this->custom_attribute  = $custom_attribute;

		// $pagination = $this->get('Pagination');
		// $this->pagination        = $pagination;
		// $this->themeticattribute = $session->get('themeticattribute');

		// workspaceHelper::updateWorkspace($innercall);
		// cssHelper::saveSldToDatabase(1, 1);

		// return parent::display($tpl);
	}
	public function getManageajax()
	{
		$session         = JFactory::getSession();
		$custolibmarray  = explode(",",$session->get('customattributelib'));
		$customattribute = $session->get('customattribute');
		$customarray     = explode(",",$customattribute);
		$finalarray      = array(array_filter($customarray), array_filter($custolibmarray));

		$i             = 0;
		$html          = '';
		$finalarray[1] = array_unique($finalarray[1]);

		foreach($finalarray[1] as $eacharray){
			if(in_array($eacharray,$finalarray[0])){
				$checked  = "checked";
				$selected = "Remove";
				$action   = "";
			}else{
				$checked  = "";
				$selected = "Select";
				$action   = "checked";
			}

			if(($i%2)==0){
				$clear = "clear";
			}else{
				$clear ="";
			}
			$namevalue = explode(":",$eacharray);


			$html .= '<tr>
		    		<td align="center">
			 			<input type="checkbox" name="variable1" class="dcv" value="'.$namevalue[1].'" id="'.$namevalue[0].'">
			 			<label>&nbsp;</label>
					</td>
					<td class="customdelete"><label for="'.$namevalue[0].'">'.$namevalue[0].'</label></td>
					<td align="center">
						<a href="javascript:void(0);" style="float:left;padding-left:5px;">
							<img src="'.JUri::base().'/components/com_mica/images/edit.png" alt="Edit" class="customedit" id="'.$namevalue[0].'" value="'.$namevalue[1].'">
						</a>
						<a href="javascript:void(0);" style="float:left;padding-left:5px;" onclick="JQuery(this).parent().prev().prev().children().attr(\'checked\',\'checked\');JQuery(\'.deletecustomvariable\').click();">
							<img src="'.JUri::base().'/components/com_mica/images/delete.png" alt="Delete">
						</a>
					</td>
				</tr>';
			$i++;
		}
		echo $html;die();
	}

	/*
		Checking user viewing data access
	 */
	public function  viewRecordPlanLimit()
	{
		$model = $this->getModel();
		$data = $model->viewRecordPlanLimit();

		echo $data; die();
	}

	/*
		Checking user downloading data access
	 */
	public function  downloadAllowedPlanLimit()
	{
		$model = $this->getModel();
		$data = $model->downloadAllowedPlanLimit();

		echo $data; die();
	}

	public function getGISDataajax()
	{

		$session                = JFactory::getSession();
		$model                  = $this->getModel('seishowresults');
		$app                    = JFactory::getApplication();

		$fromthematic           = $session->get('fromthematic');
		$tomcaturl              = TOMCAT_URL;
		$zoom                   = $this->zoom;
		$geometry               = $this->geometry;
		$longlat                = $app->input->get('longlat', '', 'raw');
		$state                  = $this->state;
		$district               = $this->district;
		$UnselectedDistrict     = $this->UnselectedDistrict;
		$districtsearchvariable = $this->districtsearchvariable;
		$town                   = $this->town;
		$townsearchvariable     = $this->townsearchvariable;
		$urban                  = $this->urban;
		$urbansearchvariable    = $this->urbansearchvariable;
		$userid                 = $this->userid;
		$activeworkspace        = $this->activeworkspace;

		$str['gis_fromthematic']           = $fromthematic;
		$str['gis_tomcaturl']              = $tomcaturl;
		$str['gis_zoom']                   = $zoom;
		$str['gis_geometry']               = $geometry;
		$str['gis_longlat']                = $longlat;
		$str['gis_state']                  = str_replace(",","','",$state);
		$str['gis_district']               = $district;
		$str['gis_UnselectedDistrict']     = $UnselectedDistrict;
		$str['gis_districtsearchvariable'] = $districtsearchvariable;
		$str['gis_town']                   = $town;
		$str['gis_townsearchvariable']     = $townsearchvariable;
		$str['gis_urban']                  = $urban;
		$str['gis_urbansearchvariable']    = $urbansearchvariable;
		$str['gis_userid']                 = $userid;
		$str['gis_activeworkspace']        = $activeworkspace;

		$str['gis_sldrail_state']          = JURI::base().TOMCAT_SLD_FOLDER.'/'.$userid.'/'.$activeworkspace.'/rail_state.sld';
		$str['gis_sldmy_table']            = JURI::base().TOMCAT_SLD_FOLDER.'/'.$userid.'/'.$activeworkspace.'/my_table.sld';
		$str['gis_sldurban']               = JURI::base().TOMCAT_SLD_FOLDER.'/'.$userid.'/'.$activeworkspace.'/jos_mica_urban_agglomeration.sld';

		$str['gis_slddistrict'] ='http://www.mica-mimi.in/'.TOMCAT_SLD_FOLDER.'/'.$userid.'/'.$activeworkspace.'/india_information.sld';

		if($app->input->get('type') == 'thematic')
		{
			$setThematicQueryData = $this->setThematicQueryData();
			$str['gis_totalthemecount']     = $setThematicQueryData['totalthemecount'];
			$str['gis_usedlevel']           = ((string)$setThematicQueryData['usedlevel'] != '')?$setThematicQueryData['usedlevel']:'';
			$str['gis_havingthematicquery'] = $setThematicQueryData['havingthematicquery'];
		}

		echo json_encode($str,true); exit;
	}

	public function setThematicQueryData() {
		$db      = JFactory::getDBO();

		$activeworkspace = $this->activeworkspace;


		$query = "SELECT * FROM ".$db->quoteName('#__mica_sld_legend')."
			WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($activeworkspace)."
			ORDER BY ".$db->quoteName('level').", ".$db->quoteName('range_from')." ASC";
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$id    = array();
		$level = array();
		$i     = 1;
		$j     = 1;
		$grouping = array();
		foreach($result as $range){
			if($range->level == "0"){
				$str = "<div class='col_".$i."' style='height:10px;width:10px;background-color:".$range->color.";float:right;'/></div></li>";
			}else{
				$pin = str_replace("#","",$range->color);
				$str = "<div class='col_".$i."' style='float:right;'/><img src='".JUri::base()."components/com_mica/maps/img/layer".$range->level."/pin".$pin.".png' /></div></li>";
			}

			$grouping[$range->custom_formula][] = "<li class='range_".$range->level."' id='".$range->range_from." - ".$range->range_to."'><div style='float:left;width:80%;'>".$range->range_from." - ".$range->range_to."</div>".$str;
			//$id[] = $range->custom_formula;
			//$grouping[$range->custom_formula] = $range->level;
			$level[$range->custom_formula][] = $range->level;
			$i++;
			$j++;
		}


		$i            = 0;
		$range        = array();
		$str          = array();
		$totalcount   = 0;
		$l            = 0;
		$tojavascript = array();
		$grpname = array();
		foreach($grouping as $key => $val){

			$grname         = $this->getCustomAttrNameThematic($key, $activeworkspace);
			$grpname[]      = $grname;
			$str[]          = implode("\n",$val);
			$ranges[]       = count($val);
			$levelunique[]  = $level[$key][0];
			$tojavascript[] = $key;
			$l++;
		}

		$tojavascript = implode(",",$tojavascript);
		$html = "";
		for($i = 0; $i < count($grpname); $i++){
			$outputstr.= "<div class='contentblock1 '>
				<div class='blockcontent'>
					<ul class='maingrp bullet-add clear' >
						<li>
							<div class='themehead'>".$grpname[$i][0]."</div>
							<div class='themeedit'>
								<a class='edittheme ".$ranges[$i]."' id='".$grpname[$i][1]."__".$levelunique[$i]."' >
									<img src='".JUri::base()."components/com_mica/images/edit.png' />
								</a>
								<a class='deletegrp ".$levelunique[$i]."' id='del_".$grpname[$i][1]."' >
									&nbsp;&nbsp;&nbsp;<img src='".JUri::base()."components/com_mica/images/delete.png' />
								</a>
							</div>
							<ul class='range' >".$str[$i]."</ul>
						</li>
					</ul>
				</div>
			</div>";
		}


		$output['totalthemecount']     = count($grpname);
		$output['usedlevel']           = implode(",",$levelunique);
		$output['havingthematicquery'] = $tojavascript;

		return $output;
	}

	public function getCustomAttrNameThematic($name,$activeworkspace) {
		$db    = JFactory::getDBO();
		$query = "SELECT name, attribute FROM ".$db->quoteName('#__mica_user_custom_attribute')."
		WHERE ".$db->quoteName('profile_id')." = ".$db->quote($activeworkspace)."
			AND ".$db->quoteName('attribute')." LIKE ".$db->quote($name);
		$db->setQuery($query);
		$result = $db->loadAssoc();

		if(count($result) == 0){
			return array($name,$name);
		}else{
			return array($result[0],$result[1]);
		}
	}
}

