<?php 
jimport('joomla.application.component.helper');
include_once 'components/com_mica/helpers/fpdf17/fpdf.php';
class pdfHelper
{
	var $_pdfobj=null;
	function __construct()
	{
		$this->_pdfobj=new FPDF('L');
		$this->_pdfobj->AliasNbPages();
	}
	function newPage()
	{	
		$this->_pdfobj->AddPage();
	}
	function addHeader($title,$logo)
	{
		// Logo
	    $this->_pdfobj->Image($logo,10,10,-300);
	    // Arial bold 15
	    $this->_pdfobj->SetFont('Arial','B',15);
	    // Move to the right
	    $this->_pdfobj->Cell(80);
	    // Title
	    $this->_pdfobj->Cell(30,10,$title,0,0,'C');
	    // Line break
	    $this->_pdfobj->Ln(20);
	}
	function addFooter()
	{
		// Position at 1.5 cm from bottom
	    $this->_pdfobj->SetY(-15);
	    // Arial italic 8
	    $this->_pdfobj->SetFont('Arial','I',8);
	    // Page number
	   // $this->_pdfobj->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}
	function addImage($imagename, $x=null, $y=null, $w=0, $h=0, $type='', $link='')
	{
		$this->_pdfobj->Image($imagename, $x=null, $y=null, $w=0, $h=0, $type='', $link='');
	}
	function addTable()
	{
		
	}
	function addHtml($h,$html)
	{	require('html_pdf.php');
		$pdf=new PDF_HTML();
		$pdf->SetFont('Arial','',12);
	 	$pdf->WriteHTML($html);

	}
	function setOutput($outputname)
	{
		$this->_pdfobj->Output($outputname,'D');
	}
}

?>
