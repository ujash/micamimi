<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/Workspace.php' );
require_once( JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers/css.php' );
/**
 * SEI view class for Mica.
 *
 * @since  1.6
 */
class MicaViewSEI extends JViewLegacy
{
	var $townsearchvariable     = "OGR_FID";
	var $urbansearchvariable    = "OGR_FID";
	var $districtsearchvariable = "OGR_FID";
	var $statesearchvariable    = "OGR_FID";
	var $tomcaturl = "";

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed   A string if successful, otherwise an Error object.
	 *
	 * @since   1.6
	 */
	public function display($tpl = null)
	{
		if(!defined('TOMCAT_URL')){
			$this->getTomcatUrl();
			DEFINE('TOMCAT_URL', $this->tomcaturl);
		}
			//DEFINE('TOMCAT_STYLE_ABS_PATH' ,"/usr/share/tomcat/webapps/geoserver/data/styles/www/data/");
			//DEFINE('TOMCAT_STYLE_ABS_PATH' , JPATH_SITE."/geoserver/data/www/styles/micamimi18/");
		

		if(!defined('TOMCAT_STYLE_ABS_PATH')){
			//DEFINE('TOMCAT_STYLE_ABS_PATH' ,"/usr/local/easy/share/easy-tomcat7/webapps/geoserver/data/www/styles/micamimi18/");
			DEFINE('TOMCAT_STYLE_ABS_PATH' ,"/usr/local/easy/share/tomcat/webapps/geoserver/data/www/styles/micamimi18/");
			//DEFINE('TOMCAT_STYLE_ABS_PATH' , JPATH_SITE."/geoserver/data/www/styles/");
		}

		if(!defined('TOMCAT_SLD_FOLDER')){
			//DEFINE('TOMCAT_SLD_FOLDER' ,"geoserver/www/styles/micamimi18");
			DEFINE('TOMCAT_SLD_FOLDER' ,"geoserver/www/styles/micamimi18/");
		}

		$app     = JFactory::getApplication();
		$session = JFactory::getSession();

		$reset = $app->input->get('reset', 0, 'int');
		if($reset == 0){
			$session->set('oldattributes', $session->get('attributes'));
		}else{
			$session->set('oldattributes', "");
			$session->set('attributes', null);
			$session->set('state', null);
			$session->set('district', null);
			$session->set('urban', null);
			$session->set('town', null);
			$session->set('activeworkspace', null);
			$session->set('attrname', null);
			$session->set('customattribute', null);
			$session->set('m_type', null);
			$session->set('activetable', null);
			$session->set('activedata' ,null);
			$session->set('composite', null);
			$session->set('m_type_rating', null);
			$session->set('customformula', null);
			$session->set('Groupwisedata', null);
			$session->set('indlvlgrps', null);
		}

		//////.//////////// Workspace ///////////////////////
		if($session->get('activeworkspace') == ""){
			$result = workspaceHelper::loadWorkspace(1);
			if(!empty($result))
			{
	    		$session->set('activeworkspace',$result[0]['id']);
				$innercall = 1;
				$session->set('gusetprofile',$result[0]['id']);
				$session->set('is_default',"1");
				$app->input->set('activeworkspace', $session->get('activeworkspace'));
				cssHelper::flushOrphandSld($session->get('activeworkspace'));
			}else
			{
				$name      = $app->input->set("name", "Guest");
				$default   = $app->input->set("default", 1);
				$innercall = 1;
				$id        = workspaceHelper::saveWorkspace($innercall);
				$session->set('gusetprofile', $id);
				$session->set('activeworkspace', $id);
				$session->set('is_default',"1");
			}
		}
		else if($session->get('is_default') == 1)
		{
			workspaceHelper::updateWorkspace($innercall);
		}
		else
		{
			$app->input->set('activeworkspace', $session->get('activeworkspace'));
		}

		$state = $app->input->get('state', '', 'raw');
		$stats = array();
		foreach($state as $eachstat)
		{
			$stats[] = base64_decode($eachstat);
		}
		$state = $stats;
		if(is_array($state)){
			$state = implode(",",$state);
			$app->input->set('state',$state);
		}
		//echo "<pre/>";print_r($session->get('state'));exit;

		$sessionstate = $session->get('state');
		//echo "<pre/>";print_r($sessionstate);exit;
		$app->input->set('state', $sessionstate);
		if($sessionstate != ""){
			$addtosession = ($sessionstate.",".$state);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp);
			$sessionstate = implode(",",$mytmp);
		}else{
			$sessionstate = $state;
		}
		$session->set('state', $sessionstate);


		$district = $app->input->get('district', '', 'raw');
		if(is_array($district)){
			$district = implode(",", $district);
			$app->input->set('district', $district);
		}

		$sessiondistrict = $session->get('district');
		if($sessiondistrict != ""){
			$addtosession1   = ($sessiondistrict.",".$district);
			$tmp             = explode(",",$addtosession1);
			$mytmp           = array_unique($tmp);
			$mytmp           = array_filter($mytmp);
			$sessiondistrict = implode(",",$mytmp);
		}else{
			$sessiondistrict = $district;
		}
		$app->input->set('district',$sessiondistrict);
		$session->set('district',$sessiondistrict);



		$m_type = $app->input->get('m_type', '', 'raw');
		if(!is_array($m_type)){
			$m_type = $session->get('m_type');
		}else{
			$session->set('m_type', $m_type);
			$m_type = $session->get('m_type');
		}


		$town        = $app->input->get('town', '', 'raw');
		$sessiontown = $session->get('town');
		if($sessiontown != ""){
			$addtosession =($sessiontown.",".$town);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp);
			$sessiontown  = implode(",",$mytmp);
		}else{
			$sessiontown = $town;
		}
		$session->set('town',$sessiontown);
		$app->input->set('town',$sessiontown);


		$urban        = $app->input->get('urban', '', 'raw');
		$sessionurban = $session->get('urban');
		if($sessionurban !="" ){
			$addtosession = ($sessionurban.",".$urban);
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$sessionurban = implode(",",$mytmp);
			$session->set('dataof',"Urban");
		}else{
			$sessionurban = $urban;
		}
		$session->set('urban',$sessionurban);
		$app->input->set('urban',$sessionurban);

		if($sessiontown != ""){
			$app->input->set('db_table','Town');
			$zoom = 9;//JRequest::setVar('db_table','Town');
		}else if($sessionurban != ""){
			$app->input->set('db_table','Urban');
			$zoom = 8;//JRequest::setVar('db_table','Urban');
		}else if($sessiondistrict != ""){
			$app->input->set('db_table','District');
			$zoom = 7;//JRequest::setVar('db_table','District');
		}else{
			$app->input->set('db_table','State');
			$zoom = 6;//JRequest::setVar('db_table','State');
		}

		$dataof = $app->input->get('dataof', '', 'raw');
		if($dataof == ""){
			$dataof = $session->get('dataof');
			//$app->input->get('dataof',$dataof);
		}else{
			$session->set('dataof',$dataof);
		}

		$composite = $app->input->get('composite', '', 'raw');
		if(!empty($composite)){
			$session->set('composite', $app->input->get('composite', '', 'raw'));
			//$attributes = JRequest::getVar('attributes');
			$composite = array_merge(array("MPI"), $composite);
		}else{
			$session->clear('composite');
		}

		$attributes = $app->input->get('attributes', '', 'raw');
		if(!empty($attributes)){
			$composite = $session->get('composite');
		    //echo  '<br/>==<br/>'.implode(",",$composite);
		    //echo  '<br/>==<br/>'.implode(",",$attributes);
			//$attributes = JRequest::getVar('attributes');
			if(!empty($composite)){
				$attributes = array_merge(array("MPI"),$attributes,$composite);
			}else{
				$attributes = array_merge(array("MPI"),$attributes);
			}
			//echo  '<br/>==<br/>'.implode(",",$attributes);
		}
		//echo "a<pre>";print_r($attributes);exit;
		if(is_array($attributes)){
			$attributes = array_filter($attributes);
			$attributes = implode(",",$attributes);
		}
		$sessionattr = $session->get('attributes');
		if($sessionattr != ""){
			if($app->input->get('attributes', '', 'raw') != ""){
				$addtosession = ($attributes);
			}else{
				$addtosession = ($sessionattr);
			}
			$tmp          = explode(",",$addtosession);
			$mytmp        = array_unique($tmp);
			$mytmp        = array_filter($mytmp, 'strlen');
			$addtosession = implode(",",$mytmp);
		}else{
			$addtosession = $attributes;
		}
		$session->set('attributes',$addtosession );
		$app->input->set('sessionattributes',$addtosession);


		$app->input->set('composite', $session->get('composite'));
		$app->input->set('m_type_rating', $session->get('m_type_rating'));
		$app->input->set('attributes', $session->get('attributes'));
		$app->input->set('state', $session->get('state'));
		$app->input->set('m_type', $session->get('m_type'));
		$app->input->set('preselected', $session->get('district'));

		$msg = $app->input->get('msg', '', 'raw');
		if($msg == "-1"){
			$app->enqueueMessage( JTEXT::_('WORKSPACE_DELETED') );
		}else if($msg == "0"){
			$app->enqueueMessage( 'WORKSPACE_CREATED' );
		}else if($msg == "1"){
			$app->enqueueMessage( 'WORKSPACE_UPDATED' );
		}

		$this->state_items         = $this->get('StateItems');
		$this->export_files        = $this->get('ExportFiles');
		$this->composite_attr      = $this->get('compositeAttr');
		$this->typesArray          = $this->get('AttributeType');
		$this->industrylevelgroups = $this->get('IndustryLevelGroups');
		$this->state      		   = $sessionstate;
		$this->district            = $sessiondistrict;
		$this->town                = $sessiontown;
		$this->urban               = $sessionurban;
		$this->m_type              = $m_type;
		$this->themeticattribute   = $session->get('themeticattribute');
		$this->user                = JFactory::getUser();
		$this->userid              = $this->user->id;
		$this->activeworkspace     = $session->get('activeworkspace');
		$this->townsearchvariable     = $this->townsearchvariable;
		$this->urbansearchvariable    = $this->urbansearchvariable;
		$this->districtsearchvariable = $this->districtsearchvariable;
		$this->zoom                 = $zoom;

		$model = JModelLegacy::getInstance('SEIShowresults', 'MicaModel', array('ignore_request' => true));
		$geometry                 = $model->getInitialGeometry();
		$this->geometry           = $geometry;
		$UnselectedDistrict       = $model->getUnselectedDistrict();
		$this->UnselectedDistrict = $UnselectedDistrict;

		$this->customdata = $model->getCustomData();
		return parent::display($tpl);
	}

	function getTomcatUrl(){
		$db    = JFactory::getDBO();
		$query = "SELECT tomcatpath FROM ".$db->quoteName('#__mica_configuration');
		$db->setQuery($query);
		$this->tomcaturl = $db->loadResult();
	}
}
