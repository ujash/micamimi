<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');

//START::Edited -Mrunal -17/06/2017 -get membership Pro plans
$db = JFactory::getDBO();
$SQL = " SELECT id, title
	FROM ".$db->quoteName('#__osmembership_plans')."
	WHERE ".$db->quoteName('published')." = ".$db->quote(1)."
	AND ".$db->quoteName('access')." IN (".implode(',', JFactory::getUser()->getAuthorisedViewLevels()).")
	ORDER BY ".$db->quoteName('ordering')." ASC ";
$db->setQuery($SQL);
$plans = $db->loadAssocList();

?>
<div class="registration<?php echo $this->pageclass_sfx; ?>">
	<!-- <?php if ($this->params->get('show_page_heading')) : ?>
		<div class="page-header">
			<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
		</div>
	<?php endif; ?> -->
	<div class="page-header">
		<h2><?php echo $this->escape($this->params->get('page_heading')); ?></h2>
	</div>
	<p style="padding-left:10px;"><?php echo JText::_('REGISTRATION_INTROTEXT'); ?></p>

	<?php //START::Edited -Mrunal -17/06/2017 -get default fields in variable to insert custom fields in layout.
	$fields = $this->form->getFieldset('default');
	?>

	<form id="member-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=registration.registermica'); ?>" method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
		<table class="contentpane" border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
				<td width="48%" valign="top">
					<div class="reg_box">
						<fieldset>
							<legend><?php echo JText::_('Basic Info'); ?></legend>
							<?php if(count($plans) > 0 ){ ?>
								<div class="control-group">
									<div class="control-label">
										<label id="jform_plan_id-lbl" for="jform_plan_id" class="hasPopover required"
											title="<?php echo JText::_( 'PLAN_NAME' ); ?>" data-content="<?php echo JText::_('Select your plan.') ?>" >
											<?php echo JText::_( 'PLAN_NAME' ); ?>
											<span class="star">&nbsp;*</span>
										</label>
									</div>
									<div class="controls">
										<div class="s_bg_select">
											<select name="jform[plan_id]" id="jform_plan_id" value="" class="required" required="required">
												<option value=""><?php echo JText::_('Please Select'); ?></option>
												<?php foreach ($plans as $key => $value){?>
													<option value="<?php echo $value['id']; ?>">
														<?php echo $value['title']; ?>
													</option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
							<?php } ?>

							<div class="control-group">
								<div class="control-label">
									<label id="jform_designation-lbl" for="jform_designation" class="hasPopover required"
										title="<?php echo JText::_( 'DESIGNATION' ); ?>" data-content="<?php echo JText::_('Select your designation.') ?>" >
										<?php echo JText::_( 'DESIGNATION' ); ?>
									</label>
								</div>
								<div class="controls">
									<div class="s_bg_select">
										<select name="jform[designation]" id="jform_designation" value="" >
											<option value="mr" >Mr.</option>
											<option value="miss" >Miss.</option>
											<option value="mrs" >Mrs.</option>
											<option value="dr" >Dr.</option>
											<option value="prof" >Prof.</option>
										</select>
									</div>
								</div>
							</div>

							<?php $field = $fields['jform_name']; ?>
							<div class="control-group">
								<div class="control-label">
									<?php echo $field->label; ?>
								</div>
								<div class="controls">
									<div class="s_bg_input">
										<?php echo $field->input; ?><span class="mandatory">*</span>
									</div>
								</div>
							</div>
							<?php $field = $fields['jform_email1']; ?>
							<div class="control-group">
								<div class="control-label">
									<?php echo $field->label; ?>
								</div>
								<div class="controls">
									<div class="s_bg_input">
										<?php echo $field->input; ?><span class="mandatory">*</span>
									</div>
								</div>
							</div>

							<?php $field = $fields['jform_password1']; ?>
							<div class="control-group">
								<div class="control-label">
									<?php echo $field->label; ?>
								</div>
								<div class="controls">
									<div class="s_bg_input">
										<?php echo $field->input; ?><span class="mandatory">*</span>
									</div>
								</div>
							</div>

							<?php $field = $fields['jform_password2']; ?>
							<div class="control-group">
								<div class="control-label">
									<?php echo $field->label; ?>
								</div>
								<div class="controls">
									<div class="s_bg_input">
										<?php echo $field->input; ?><span class="mandatory">*</span>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
				</td>
				<td width="4%">&nbsp;</td>
				<td width="48%" valign="top">
					<div class="reg_box">
						<fieldset>
							<legend><?php echo JText::_('Address'); ?></legend>
							<div class="control-group">
								<div class="control-label">
									<label id="jform_institute_name-lbl" for="jform_institute_name" class="hasPopover required"
										title="<?php echo JText::_( 'Company' ); ?>" data-content="<?php echo JText::_('Enter your company.') ?>" >
										<?php echo JText::_( 'Company' ); ?>
										<span class="star">&nbsp;*</span>
									</label>
								</div>
								<div class="controls">
									<div class="s_bg_input">
										<input type="text" name="jform[institute_name]" id="jform_institute_name" value="" class="required" size="40" required="required" >
										<span class="star">&nbsp;*</span>
									</div>
								</div>
							</div>
							<div class="control-group textarea">
								<div class="control-label">
									<label id="jform_address-lbl" for="jform_address" class="hasPopover"
										title="<?php echo JText::_( 'ADDRESS' ); ?>" data-content="<?php echo JText::_('Enter your address.') ?>" >
										<?php echo JText::_( 'ADDRESS' ); ?>
									</label>
								</div>
								<div class="controls">
									<div class="textarea_bg">
										<textarea name="jform[address]" id="jform_address" value="" ></textarea>
									</div>
								</div>
							</div>
							<div class="control-group">
								<div class="control-label">
									<label id="jform_mobile-lbl" for="jform_mobile" class="hasPopover required"
										title="<?php echo JText::_( 'MOBILE' ); ?>" data-content="<?php echo JText::_('Enter your mobile.') ?>" >
										<?php echo JText::_( 'MOBILE' ); ?>
										<span class="star">&nbsp;*</span>
									</label>
								</div>
								<div class="controls">
									<div class="s_bg_input">
										<input type="text" name="jform[mobile]" id="jform_mobile" value="" class="required" size="40" required="required" maxlength="10"/>
										<span class="star">&nbsp;*</span>
									</div>
								</div>
							</div>
							<div class="control-group">
								<div class="control-label">
									<label id="jform_contactnumber-lbl" for="jform_contactnumber" class="hasPopover required"
										title="<?php echo JText::_( 'CONTACT_NUMBER' ); ?>"	data-content="<?php echo JText::_('Enter your contact number.') ?>" >
										<?php echo JText::_( 'CONTACT_NUMBER' ); ?>
										<span class="star">&nbsp;*</span>
									</label>
								</div>
								<div class="controls">
									<div class="s_bg_input">
										<input type="text" name="jform[contactnumber]" id="jform_contactnumber" value="" class="required" size="40" required="required" />
										<span class="star">&nbsp;*</span>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<div class="reg_box height_small">
						<fieldset>
							<legend><?php echo JText::_('Payment'); ?></legend>
							<div class="control-group">
								<div class="control-label">
									<label id="jform_paymentoption-lbl" for="jform_paymentoption" class="hasPopover required"
										title="<?php echo JText::_( 'PAYMENT_OPTION' ); ?>"	data-content="<?php echo JText::_('Enter your payment option.') ?>" >
										<?php echo JText::_( 'PAYMENT_OPTION' ); ?>
									</label>
								</div>
								<div class="controls">
									<div class="s_bg_select">
										<select name="jform[paymentoption]" id="jform_paymentoption" class="required">
											<option value="1">Cheque</option>
											<option value="2">DD</option>
										</select>
									</div>
									<span class="mandatory">*</span>
								</div>
							</div>
							<div class="control-group">
								<div class="control-label">
									<label id="jform_chequeno-lbl" for="jform_chequeno" class="hasPopover required"
										title="<?php echo JText::_( 'CHEQUE_NUMBER' ); ?>"		data-content="<?php echo JText::_('Enter your cheque number.') ?>" >
										<?php echo JText::_( 'CHEQUE_NUMBER' ); ?>
									</label>
								</div>
								<div class="controls">
									<div class="s_bg_input">
										<input type="text" name="jform[chequeno]" id="jform_chequeno" value="" class="required" size="40" required="required" />
										<span class="mandatory">*</span>
									</div>
								</div>
							</div>
							<div class="control-group">
								<div class="control-label">
									<label id="jform_bankname-lbl" for="jform_bankname" class="hasPopover required"
										title="<?php echo JText::_( 'BANK_NAME' ); ?>" data-content="<?php echo JText::_('Enter your bank name.') ?>" >
										<?php echo JText::_( 'BANK_NAME' ); ?>
									</label>
								</div>
								<div class="controls">
									<div class="s_bg_input">
										<input type="text" name="jform[bankname]" id="jform_bankname" value="" class="required" size="40" required="required" />
										<span class="mandatory">*</span>
									</div>
								</div>
							</div>
							<div class="control-group">
								<div class="control-label">
									<label id="jform_bankname-lbl" for="jform_bankname" class="hasPopover required"
										title="<?php echo JText::_( 'PAYMENT_DATE' ); ?>"	data-content="<?php echo JText::_('Enter your payment date.') ?>" >
										<?php echo JText::_( 'PAYMENT_DATE' ); ?>
									</label>
								</div>
								<div class="controls">
									<?php echo JHTML::_('calendar', '' ,'jform[date]','jform_date',$format = '%Y-%m-%d',array('class'=>'inputbox required','size'=>'35','maxlength'=>'20','style'=>'height:24px;')); ?>
									<span class="mandatory">*</span>
								</div>
							</div>
						</fieldset>
					</div>
				</td>
				<td></td>
				<td valign="top">
					<div class="reg_box height_small">
						<fieldset>
							<legend><?php echo JText::_('WHYWANTTOBUY'); ?></legend>
							<div class="textarea_bg_big">
								<textarea name="jform[whywnattobuy]" id="jform_whywnattobuy" value="" ></textarea>
							</div>
						</fieldset>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="3" height="40">
					<span style="color:#a93f3c;">Fields marked with an asterisk (*) are required.</span>
				</td>
			</tr>
			<tr>
				<td colspan="3" height="40">
					<div class="">
						<button type="submit" class="validate"><?php echo JText::_('JREGISTER'); ?></button>
						<input type="hidden" name="option" value="com_users" />
						<input type="hidden" name="task" value="registration.registermica" />
					</div>
				</td>
			</tr>
		</table>
		<?php echo JHtml::_('form.token'); ?>
	</form>
</div>
