<?xml version="1.0" encoding="ISO-8859-1"?>
			<StyledLayerDescriptor version="1.0.0"
 xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
 xmlns="http://www.opengis.net/sld"
 xmlns:ogc="http://www.opengis.net/ogc"
 xmlns:xlink="http://www.w3.org/1999/xlink"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><NamedLayer>
			<Name><![CDATA[india_information]]></Name>
			<UserStyle>
			<Title>s0</Title>
			<FeatureTypeStyle>
			
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Valsad]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccccff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[The Dangs]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccccff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Surendranagar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccccff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Panch Mahals]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccccff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Navsari]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccccff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Narmada]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccccff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Kheda]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccccff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Jamnagar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccccff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Bharuch]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccccff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Banas Kantha]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#ccccff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Vadodara]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#d6d6ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Narmada]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#d6d6ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Kheda]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#d6d6ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Kachchh]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#d6d6ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Junagadh]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#d6d6ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Gandhinagar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#d6d6ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Surat]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#e0e0ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Sabar Kantha]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#e0e0ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Porbandar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#e0e0ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Rajkot]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#eaeaff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Mahesana]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#eaeaff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Anand]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#eaeaff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Amreli]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#eaeaff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Patan]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#f4f4ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Bhavnagar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#f4f4ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Ahmadabad]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PolygonSymbolizer><Fill><CssParameter name="fill">#f4f4ff</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Sabar Kantha]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin5.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Porbandar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin5.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Bhavnagar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin5.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Bharuch]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin5.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Anand]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin5.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Valsad]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin4.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Sabar Kantha]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin4.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Kheda]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin4.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Junagadh]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin4.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Amreli]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin4.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Vadodara]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin3.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Surendranagar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin3.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Patan]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin3.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Navsari]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin3.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Kachchh]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin3.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Jamnagar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin3.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[The Dangs]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin2.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Surat]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin2.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Rajkot]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin2.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Panch Mahals]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin2.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Mahesana]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin2.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Gandhinagar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin2.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Narmada]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin1.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Banas Kantha]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin1.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Ahmadabad]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer1/pin1.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Vadodara]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star5.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Porbandar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star5.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Kheda]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star5.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Jamnagar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star5.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[The Dangs]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star4.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Junagadh]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star4.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Anand]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star4.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Panch Mahals]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star3.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Navsari]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star3.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Narmada]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star3.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Kachchh]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star3.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Bhavnagar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star3.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Amreli]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star3.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Surendranagar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star2.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Sabar Kantha]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star2.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Patan]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star2.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Gandhinagar]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star2.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Valsad]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star1.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Surat]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star1.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Rajkot]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star1.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Mahesana]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star1.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Bharuch]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star1.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Banas Kantha]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star1.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
				<Rule>
				<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA[distshp]]></ogc:PropertyName>
			<ogc:Literal><![CDATA[Ahmadabad]]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter><PointSymbolizer>

	 <Graphic>
		<ExternalGraphic>
		   <OnlineResource xlink:type="simple" xlink:href="http://mica-mimi.in/geoserver/www/openlayers/img/layer2/map/star1.png" />
		   <Format>image/png</Format>
		</ExternalGraphic>
	 </Graphic>
	<Geometry>
	   <ogc:Function name="centroid">
		 <ogc:PropertyName>india_information</ogc:PropertyName>
	   </ogc:Function>
	 </Geometry>
 </PointSymbolizer><TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>

		 <Font>
		   <CssParameter name='font-family'>Dialog.plain</CssParameter>
		   <CssParameter name='font-size'>10</CssParameter>
		   <CssParameter name='font-style'>normal</CssParameter>

		 </Font>
		 <LabelDISTSHPment>
		   <PointDISTSHPment>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointDISTSHPment>
		 </LabelDISTSHPment>
		 <Halo>
		  <Fill>
			<CssParameter name='fill'>#FDFDFD</CssParameter>
			<CssParameter name='fill-opacity'>0.20</CssParameter>
		  </Fill>
	  </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
		 </TextSymbolizer>
				</Rule>
			</FeatureTypeStyle>
			</UserStyle>
			</NamedLayer></StyledLayerDescriptor>